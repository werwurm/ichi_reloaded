# this query extracts dates, symbol name, close and ADX for a specific symbol
# use it standalone or with sql2csv.sh

SELECT returns FROM 
	(SELECT * FROM quotes_daily WHERE symbol='EURJPY' ORDER BY date DESC LIMIT 100)      
sub ORDER BY date ASC;
