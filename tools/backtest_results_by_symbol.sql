# this query extracts the orderbook. For each roundtrip (buy and sell) one line will be created: 
# buydate,symbol,price-P_L_piece,date,price,P_L_percent,hold_days
# This query does some statistics on the orderbook: It gives the Profit/Loss (by percent) and the number of trades for the
# years specified, ordered by years and symbols
# use it standalone or with sql2csv.sh

SELECT 
    YEAR(date) AS `year`, symbol, ROUND(SUM(P_L_percent),4) AS `Profit by percent`,
    count(*) AS `nr. of trades`  FROM orderbook_daily
    WHERE date BETWEEN '2006-01-01' AND '2016-11-01' AND buy_sell='sell' GROUP BY YEAR(date), symbol;
