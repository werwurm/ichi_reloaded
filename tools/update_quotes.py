import MySQLdb
import configparser
import pandas
from pathlib import Path

# find config path
path = Path(__file__).resolve().parents[1]
configpath = str(path) + '/config/'

# read otrasys config
configfile = configpath + 'otrasys.conf'
configParser = configparser.ConfigParser(inline_comment_prefixes=('#'))
configfilepath = configfile
configParser.read(configfilepath)

hostname = configParser.get('Database', 'DB_QUOTES_HOSTNAME')
db_username = configParser.get('Database', 'DB_QUOTES_USERNAME')
db_passwd = configParser.get('Database', 'DB_QUOTES_PASSWORD')
db_name = configParser.get('Database', 'DB_QUOTES_NAME')

db = MySQLdb.connect (host = hostname, 
			user = db_username,
			passwd = db_passwd,
			db= db_name)

print(db)

#define cursor for all db operations
cur = db.cursor()

symbolfile = configParser.get('General', 'CONFIG_SYMBOLFILE')
symbolfile = configpath + symbolfile

# read symbols file into pandas dataframe
symbols = pandas.read_csv(symbolfile,
                        comment='#', delimiter=';\s*',
                        names=["symbol", "identifier", "filename","tradeable?","ticks/pips", "contract size" , "weight", "cur", "type", "comment"],
                        engine='python')

# iterate through all symbols and prepare mysql statement    
for row in symbols.itertuples():
    fullFilename = str(path) + '/data/' + row.filename
    MySQLString = 'LOAD DATA LOCAL INFILE \'' + fullFilename + '\' INTO TABLE quotes_daily FIELDS TERMINATED BY \',\' ENCLOSED BY \',\' LINES TERMINATED BY \'\\n\' (date, open, high, low, close, volume, returns) set symbol=\'' + row.symbol + '\', identifier=\'' + row.identifier + '\';'    
    cur.execute(MySQLString) 

# calculate daynumbers for each date
cur.execute("UPDATE quotes_daily SET daynr = (SELECT datediff(date, '1900-01-01'))+1")

# makes changes persistent in db
db.commit()

#cur.execute("SELECT * from quotes_daily limit 10")
#
#for row in cur.fetchall():
#    print(row[0], row[1], row[2], row[3], row[4], row[5], row[6])
#

db.close()
