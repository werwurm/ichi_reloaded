#  OTraSys- The Open Trading System
## An open source framework to create trading systems

https://gitlab.com/werwurm/OTraSys.git  

<img src="docs/pics/logo.png" alt="OTraSys Logo" style="width:100px ; float:left"/>
This Repository hosts the Open Trading System.  
It is an open source framework to create trading systems, written in C from
scratch. The goal is to create a framework for designing trading systems. 
The Ichimoku system will be the first incarnation of this framework.

In its current state, the software is used to generate all signals and charts
seen on https://spare-time-trading.de (available in English and German).  

--------------------------------------------------------------------------------------
Licence
-------
This program is Free Software: You can use, study share and improve 
it at your will. Specifically you can redistribute and/or modify it under the 
terms of the GNU General Public License as published by the Free Software 
Foundation, either version 3 of the License, or (at your option) any later version.

**Copyright 2016-2021 Denis Zetzmann, d1z@gmx.de**

Folder structure of repository: 
-------------------------------
```  
|                   main folder, hosts README.md, makefile, licence and binary after compilation   
|--build            not syncronised, hosts the object files after compilation   
|--config           hosts various config files for binary program   
|--data             not syncronised, hosts data files for binary program   
   |--charts        hosts gnuplot files and csv-files for plotting
|--docs             hosts documentation for users
   |--doxygen_html  source code documentation (for developers)
   |--pics          sample pictures and screenshots for documentation
|--src              hosts source files and headers   
|--tools            hosts several helper scripts (e.g. data retrieval, test procedures)
```
  
Build instructions:
-------------------
Just issue `make` at the main folder, where the makefile is located.
Compiled files will be generated into *build*-folder, the executable `otrasys`
will be generated in the main folder. Execute `make clean` to remove all compiled 
files and executable.
A `make debug` will generated an executable with most compiler optimizations turned
of, and including debugging symbols. Please use that if you want to report bugs (as
only debug builds are able to track down the location of the bug). 

You will need `libmysqlclient-devel` installed. The also needed better string library
(please visit http://bstring.sourceforge.net/) is included and part of src/

Usage:
------
After compiling copy `config/otrasys.conf.EXAMPLE` to `config/otrasys.conf`
and tailor to your needs. Most options are set within the config file.  
<!-- `./otrasys -?` lists the command line options. Note that with `--setup_db` or  -->
`-s` you can setup the neccessary database and tables. You will need the mysql root
password to do this (alternatively you can create the database yourself, as specified
in `docs/database_structure.md`).  
You can find an in-depth explanation for all command line options and config parameters 
in the documentation under `/docs/database_structure.md` or in the project wiki under  
https://gitlab.com/werwurm/OTraSys/wikis/config-file-and-command-line-options  

Current Build Status:
---------------------
**master branch**   [![build status](https://gitlab.com/werwurm/OTraSys/badges/master/pipeline.svg)](https://gitlab.com/werwurm/OTraSys/commits/master)  
**release branch**  [![build status](https://gitlab.com/werwurm/OTraSys/badges/release/pipeline.svg)](https://gitlab.com/werwurm/OTraSys/commits/release)  
**dev branch**      [![build status](https://gitlab.com/werwurm/OTraSys/badges/dev/pipeline.svg)](https://gitlab.com/werwurm/OTraSys/commits/dev)  

Sample Screenshots:
-------------------
While the system itself is terminal only, there won't be too impressive screenshots. 
However, the small samples below give you a hint, what to expect. For more information
please see the docs.
Please note: all screenshots show random examples taken during development, not final
optimized trading systems that are out in the wild making big money. The large versions
of those screenshots are in `docs/pics/`
### Sample terminal output:
<img src="docs/pics/terminal_output.png" alt="sample terminal output" style="width:1000px"/>  

### Sample performance using the script under `tools/`
<img src="docs/pics/performance.png" alt="sample performance graph" style="width:800px"/>  

### Sample chart
<img src="docs/pics/DAX_contrast.png" alt="sample Ichimoku Chart" style="width:800px"/>  

### Sample report
<img src="docs/pics/report.png" alt="sample terminal report" style="width:800px"/>  
