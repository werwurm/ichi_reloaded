/* statistics.c
 * implements various statistics for orderbook in mysql
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 

/**
 * @file statistics.c
 * @brief implements various statistics for orderbook in mysql
 *
 * This file implements various statistics for orderbook in mysql
 * @author Denis Zetzmann
 */

#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#include "arrays.h"
#include "statistics.h"
#include "database.h"
#include "arrays.h"
#include "date.h"
#include "readconf.h"

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief statistic reports after evaluation of orderbook 
 *
 * This function calulates some MOE/MOP by analyzing the orderbook entries
 * 
 * @return 0 if successfull, 1 otherwise
 */
///////////////////////////////////////////////////////////////////////////////
int statistics_manager(class_accounts *the_account)
{
    extern parameter parms; // parms are declared global in main.c
	unsigned int trades_total, win_trades_total, lose_trades_total;
	unsigned int trades_long, win_trades_long, lose_trades_long;
	unsigned int trades_short, win_trades_short, lose_trades_short;
	
	unsigned int hold_days_sum = 0;
	unsigned int *hold_days_array; // sorted hold days of all trades
	float hold_days_avg = 0;	// mean hold days
	float hold_days_med = 0;	// median hold days

	int consecutive_wins = 0;	// current winning streak
	int consecutive_losses = 0;	// current loosing streak
	int max_consecutive_wins = 0;	// largest winning streak
	int max_consecutive_losses = 0;	// largest loosing streak
	
	float percent_win_total, percent_win_long, percent_win_short;
	
	float win_total, win_long, win_short;
	float loss_total, loss_long, loss_short;
	
	float avg_win_total, avg_loss_total;
	float avg_win_long, avg_loss_long;
	float avg_win_short, avg_loss_short;
	
	float PF_total, PF_long, PF_short;
	float exp_total, exp_long, exp_short;
	bstring winlose_string = NULL;
	
	float *P_L = NULL; // array which will store the daily wins/losses
	
	bstring *tradedates = NULL;	// holds the date for each trade
	bstring *symbols = NULL;	// holds the symbol for each trade
	
	winlose_string = bfromcstr("all");
	trades_total = db_mysql_get_nr_trades(winlose_string, undefined);
	trades_long = db_mysql_get_nr_trades(winlose_string, longsignal);
	trades_short = db_mysql_get_nr_trades(winlose_string, shortsignal);
	
	if(trades_total == 0)	// there are no trades yet
	{
		printf("\nOrderbook statistics");
		printf("\n====================");
		printf("\n\nno trades in database!\n\n");
		return 0;	// exit this routine to prevent segfault
	}
	
	winlose_string = bfromcstr("win");
	win_trades_total = db_mysql_get_nr_trades(winlose_string, undefined);
	win_trades_long = db_mysql_get_nr_trades(winlose_string, longsignal);
	win_trades_short = db_mysql_get_nr_trades(winlose_string, shortsignal);
	win_total = db_mysql_get_PL_sum(winlose_string, undefined);
	win_long = db_mysql_get_PL_sum(winlose_string, longsignal);
	win_short = db_mysql_get_PL_sum(winlose_string, shortsignal);
	
	winlose_string = bfromcstr("lose");
	lose_trades_total = db_mysql_get_nr_trades(winlose_string, undefined);
	lose_trades_long = db_mysql_get_nr_trades(winlose_string, longsignal);
	lose_trades_short = db_mysql_get_nr_trades(winlose_string, shortsignal);
	loss_total = db_mysql_get_PL_sum(winlose_string, undefined);
	loss_long = db_mysql_get_PL_sum(winlose_string, longsignal);
	loss_short = db_mysql_get_PL_sum(winlose_string, shortsignal);
		
	percent_win_total = (float) win_trades_total / (float) trades_total;
	percent_win_long = (float) win_trades_long / (float) trades_long;
	percent_win_short = (float) win_trades_short / (float) trades_short;
	
	// nomenclature taken from https://www.amibroker.com/guide/h_report.html
	
	// average wins
	avg_win_total = win_total / (float) win_trades_total;
	avg_win_long = win_long / (float) win_trades_long;
	avg_win_short = win_short / (float) win_trades_short;
	
	// average losses
	avg_loss_total = loss_total / (float) lose_trades_total;
	avg_loss_long = loss_long / (float) lose_trades_long;
	avg_loss_short = loss_short / (float) lose_trades_short;
	
	// Profit factor: avg win/avg loss
	PF_total = avg_win_total / ((-1) * avg_loss_total);
	PF_long = avg_win_long / ((-1) * avg_loss_long);
	PF_short = avg_win_short / ((-1) * avg_loss_short);
	
	// expectancy: % Profit of winners + % Loss of losers)/(number of trades)
	// represents expected percent gain/loss per trade
	exp_total = (win_total + loss_total) / (float) trades_total;
	exp_long = (win_long + loss_long) / (float) trades_long;
	exp_short = (win_short + loss_short) / (float) trades_short;
	
	tradedates = init_1d_array_bstring(trades_total);
	symbols = init_1d_array_bstring(trades_total);
	P_L = init_1d_array_float(trades_total);
	hold_days_array = init_1d_array_uint(trades_total);	
	
	// load hold days into array
	db_mysql_get_hold_days_ordered(hold_days_array);
	
	// get all profits/losses from database
	db_mysql_get_PL(bfromcstr("P_L_total"),symbols, P_L, tradedates);

	// set up win/loss counter depending on 1st trade
	if(P_L[0] < 0)
	{
		consecutive_losses = 1;
		max_consecutive_losses = 1;
	}
	else if(P_L[0] > 0)
	{
		consecutive_wins = 1;
		max_consecutive_wins = 0;
	}
	
	// take first hold day
	hold_days_sum = hold_days_array[0];
		
	// now add all other trades
	unsigned int trade;
	for(trade=1; trade<trades_total; trade++)
	{
		// count consecutive wins/losses
		if(P_L[trade] > 0)
		{
			consecutive_losses = 0;
			consecutive_wins++;
		}
		else if(P_L[trade] < 0)
		{
			consecutive_wins = 0;
			consecutive_losses++;
		}
		// check if current win/loss streak is a new record
		if(consecutive_wins > max_consecutive_wins)
			max_consecutive_wins = consecutive_wins;
		else if(consecutive_losses > max_consecutive_losses)
			max_consecutive_losses = consecutive_losses;
		
		// sum up all holding days
		hold_days_sum = hold_days_sum + hold_days_array[trade];
	}
	
    // average (mean) holding days
	hold_days_avg = (float) hold_days_sum / (float) trades_total;
	
 	if(trades_total % 2 == 0)	// trades_total is even number
 	{
 		hold_days_med = hold_days_array[trades_total / 2 - 1];
		hold_days_med = hold_days_med + hold_days_array[trades_total / 2];
		hold_days_med = hold_days_med / 2;
 	}
 	else				// trades_total is odd number
		hold_days_med = hold_days_array[trades_total / 2];
	
    class_quotes* total = NULL;
    class_quotes* total_high = NULL;
    bstring the_symbol = bfromcstr("performance");
    bstring the_table = bfromcstr("performance_record");
    unsigned int nr_days = db_mysql_getNrOfQuotes(the_symbol, the_symbol, the_symbol, the_table);
    
    total = class_quotes_init("performance", "performance", nr_days, the_account->nr_digits, "total", bdata(the_table));
    total_high = class_quotes_init("performance", "performance", nr_days, the_account->nr_digits, "total_high", bdata(the_table));

    total->loadFromDB(total);
    total_high->loadFromDB(total_high);
    
    bdestroy(the_symbol); bdestroy(the_table);
    
    unsigned int days_in_drawdwn = 0;
    unsigned int longest_drawdwn = 0;
    unsigned int days_in_curr_drawdwn = 0;
    unsigned int nr_drawdwns = 0;
    float largest_drawdwn = 0;
    bool new_drawdwn = true;
    
    for(unsigned int day_idx = 0; day_idx < nr_days; day_idx++)
    {
        float todays_delta = total->quotevec[day_idx] - total_high->quotevec[day_idx];
        if(todays_delta < 0)
        {
            days_in_drawdwn++;
            days_in_curr_drawdwn++;
            
            if(todays_delta < largest_drawdwn)
                largest_drawdwn  = todays_delta;
            if(days_in_curr_drawdwn > longest_drawdwn)
                longest_drawdwn = days_in_curr_drawdwn;
            if(new_drawdwn)
            {
                days_in_curr_drawdwn = 1;
                nr_drawdwns++;
                new_drawdwn = false;
            }
        }
        else
        {
            new_drawdwn = true;
        }
    }
    printf("\nOrderbook statistics");
	printf("\n====================");
    if(parms.SIGNAL_EXECUTION_LONG && parms.SIGNAL_EXECUTION_SHORT)
    {
        printf("\n\t\t\tALL\t\tLONG\t\tSHORT");
        printf("\n\t\t\t---\t\t----\t\t-----");
        printf("\nNumber of trades:\t%i\t\t%i\t\t%i", trades_total, trades_long, trades_short);    
        printf("\nNumber of winners:\t%i\t\t%i\t\t%i", win_trades_total, win_trades_long, win_trades_short);
        printf("\nNumber of losers:\t%i\t\t%i\t\t%i", lose_trades_total, lose_trades_long,lose_trades_short);
        printf("\nWin percentage: \t%.2f%%\t\t%.2f%%\t\t%.2f%%", percent_win_total*100, percent_win_long*100, percent_win_short*100);
        printf("\n");
        printf("\nOverall Win/loss (%s):\t%.2f\t\t%.2f\t\t%.2f", bdata(*the_account->currency), (win_total + loss_total), (win_long + loss_long), (win_short + loss_short));
        printf("\nTotal Win (%s):\t%.2f\t%.2f\t%.2f", bdata(*the_account->currency), win_total, win_long, win_short);
        printf("\nTotal Loss (%s):\t%.2f\t%.2f\t%.2f", bdata(*the_account->currency), loss_total, loss_long, loss_short);
        printf("\navg win (%s):\t\t%.2f\t\t%.2f\t\t%.2f", bdata(*the_account->currency), avg_win_total, avg_win_long, avg_win_short);
        printf("\navg loss (%s):\t\t%.2f\t\t%.2f\t\t%.2f", bdata(*the_account->currency), avg_loss_total, avg_loss_long, avg_loss_short);
        printf("\nProfit Factor (avg w/l):%.2f\t\t%.2f\t\t%.2f", PF_total, PF_long, PF_short);
        printf("\nExpectancy (%s/trade):\t%.2f\t\t%.2f\t\t%.2f", bdata(*the_account->currency), exp_total, exp_long, exp_short);
    }
    else if(parms.SIGNAL_EXECUTION_LONG && !parms.SIGNAL_EXECUTION_SHORT)
    {
        printf("\n\t\t\tLONG only");
        printf("\n\t\t\t---------");
    	printf("\nNumber of trades:\t%i",trades_long);
        printf("\nNumber of winners:\t%i", win_trades_long);
        printf("\nNumber of losers:\t%i", lose_trades_long);
        printf("\nWin percentage: \t%.2f%%", percent_win_long*100);
        printf("\n");
        printf("\nOverall Win/loss (%s):\t%.2f", bdata(*the_account->currency), (win_long + loss_long));
        printf("\nTotal Win (%s):\t%.2f", bdata(*the_account->currency), win_long);
        printf("\nTotal Loss (%s):\t%.2f", bdata(*the_account->currency), loss_long);
        printf("\navg win (%s):\t\t%.2f", bdata(*the_account->currency),  avg_win_long);
        printf("\navg loss (%s):\t\t%.2f", bdata(*the_account->currency),  avg_loss_long);
        printf("\nProfit Factor (avg w/l):%.2f", PF_long);
        printf("\nExpectancy (%s/trade):\t%.2f", bdata(*the_account->currency), exp_long);
    }
    else if(!parms.SIGNAL_EXECUTION_LONG && parms.SIGNAL_EXECUTION_SHORT)
    {
        printf("\n\t\t\tSHORT only");
        printf("\n\t\t\t----------");
        printf("\nNumber of trades:\t%i", trades_short);
        printf("\nNumber of winners:\t%i", win_trades_short);
        printf("\nNumber of losers:\t%i", lose_trades_short);
        printf("\nWin percentage: \t%.2f%%", percent_win_short*100);
        printf("\n");
        printf("\nOverall Win/loss (%s):\t%.2f", bdata(*the_account->currency), (win_short + loss_short));
        printf("\nTotal Win (%s):\t%.2f", bdata(*the_account->currency), win_short);
        printf("\nTotal Loss (%s):\t%.2f", bdata(*the_account->currency), loss_short);
        printf("\navg win (%s):\t\t%.2f", bdata(*the_account->currency),  avg_win_short);
        printf("\navg loss (%s):\t\t%.2f", bdata(*the_account->currency),  avg_loss_short);
        printf("\nProfit Factor (avg w/l):%.2f", PF_short);
        printf("\nExpectancy (%s/trade):\t%.2f", bdata(*the_account->currency), exp_short);
    }
    printf("\n");
    printf("\nFirst Trade (roundtrip):%s\tLast Trade:\t%s", bdata(tradedates[0]), bdata(tradedates[trades_total-1]));
    printf("\nNr. Drawdowns: \t\t%i\t\tDays in drawdown:\t%i", nr_drawdwns, days_in_drawdwn);
    printf("\nMax days in drawdown:\t%i\t\tavg days in drawdown:\t%.1f", longest_drawdwn, (float) days_in_drawdwn / (float) nr_drawdwns);
    printf("\nLargest Drawdown:\t%.2f", largest_drawdwn);
    printf("\nmax consecutive loose trades:\t%i\tmax consecutive win trades:%i", max_consecutive_losses, max_consecutive_wins);
    printf("\nholding days (mean avg):\t%.1f\tholding days (median): %.1f", hold_days_avg, hold_days_med);
    printf("\n");
   
    the_account->printTable(the_account);
    
    total->destroy(total);
    total_high->destroy(total_high);
    
	free_1d_array_float(P_L);
	free_1d_array_uint(hold_days_array);
	
	free_1d_array_bstring(tradedates, trades_total);
	free_1d_array_bstring(symbols, trades_total);
	bdestroy(winlose_string);
	
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief calculates arithmetic mean of values
 *
 * This function calulates x_roof = 1/N * sum(x_i) 
 * 
 * @param values float ptr to 1xN vector of values
 * @param number number of values in vector
 * @return arithmeticMean as float
 */
///////////////////////////////////////////////////////////////////////////////
float calc_arithmeticMean(float *values, int number)
{
    float mean = 0.0;
    if(number)  // catch the case that number is 0
    {
        for(int i = 0; i<number; i++)
        {
            mean = mean + values[i];
        }
        
        mean = mean / number;
    }
    return mean;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief calculates standard deviation of values
 *
 * This function calulates \sigma= \sqrt( 1/(N-1) \sum(x_i - x^roof)²)
 * 
 * @param values float ptr to 1xN vector of values
 * @param mean value of 1xN vector
 * @param number number of values in vector
 * @return sigma as float
 */
 float calc_standardDeviation(float *values, float mean, int number)
 {
	float standardDeviation = 0.0;
	
	for(int i=0; i<number; i++)
	{
		standardDeviation = standardDeviation + (values[i] - mean) * (values[i] - mean);
	}
	standardDeviation = 1/( (float) number-1) * standardDeviation;
	standardDeviation = sqrt(standardDeviation);
	return standardDeviation;
 }
 
///////////////////////////////////////////////////////////////////////////////
/**
 * @brief calculates svariance of values
 *
 * This function calulates \sigma= 1/(N-1) \sum(x_i - x^roof)²
 * 
 * @param values float ptr to 1xN vector of values
 * @param mean value of 1xN vector
 * @param number number of values in vector
 * @return variance as float
 */
float calc_variance(float *values, float mean, int number)
 {
	float variance = 0.0;
	
	for(int i=0; i<number; i++)
	{
		variance = variance + (values[i] - mean) * (values[i] - mean);
	}
	variance = 1/( (float) number-1) * variance;

	return variance;
 }
 
 
///////////////////////////////////////////////////////////////////////////////
/**
 * @brief calculates covariance between 2 float vectors
 *
 * This function calulates the covariance of 2 vectors using
 * cov(x,y) = \frac{\sum_(i=1)^N (x_i - x_mean)*(y_i - y_mean)} {N-1}
 * 
 * @param values_x float ptr to 1xN vector of values for x
 * @param values_y float ptr to 1xN vector of values for x
 * @param x_mean arithmetic mean of x
 * @param y_mean arithmetic mean of y
 * @param number number of values in vectors
 * @return covariance as float
 */ 
///////////////////////////////////////////////////////////////////////////////
float calc_covariance(float *values_x, float *values_y, float x_mean, float y_mean, int number)
{
    float cov = 0.0;
    
    for(int i=0; i<number; i++)
    {
//         printf("\n(%.8f - %.8f) * (%.8f - %.8f)", values_x[i] , x_mean ,values_y[i] , y_mean);
        cov = cov + (values_x[i] - x_mean) * (values_y[i] - y_mean);
    }
    
    cov = cov / (number - 1);
    
    return cov;
}


///////////////////////////////////////////////////////////////////////////////
/**
 * @brief calculates Covariance Matrix for given data
 *
 * This function calculates the Covariance Matrix for the given data set. Note
 * that the means and variances are not calculated in this function but have
 * to be given as parameters
 * 
 * @param row_cols uint nr of rows/cols of resulting (symmetric covariance matrix,
 *          equals the nr. of individual variables
 * @param nr_values unit nr of values for each variable
 * @param CovMat float ptr to NxN Covariance Matrix, results are stored here,
 *          original values will be overwritten
 * @param variables float ptr to NxM matrix, N=row_cols, M=nr_values, contains
 *          all data
 * @param means float ptr to Nx1 vector containing arithmetic means of all variables
 * @param variances float ptr to Nx1 vector containing variances of all variables
 * @param period uint period (covariance is scalable)
*/
///////////////////////////////////////////////////////////////////////////////
void calc_covariance_matrix(unsigned int row_cols, unsigned int nr_values, float **CovMat, float **variables, float* means, float* variances, unsigned int period)
{   
    for(unsigned int i=0; i < row_cols; i++)
        for(unsigned int j=0; j<= i; j++)
        {
            // main diagonal entries of Covariance Matrix is variance 
            if(i==j)
            {
                CovMat[i][i] = variances[i];
            }
            else
            {
                float cov = calc_covariance(variables[i], variables[j], means[i], means[j], nr_values);
                // set cov entries within the period (matrix is symmetric)
                cov = cov * period;
                CovMat[i][j] = cov;
                CovMat[j][i] = cov;
            }            
        }
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief calculates inverse of a NxN matrix
 *
 * This function calulates the the inverse of a NxN Matrix using the Gauß-
 * Jordan algorithm.
 * 
 * @note based on code found here:
 * https://stackoverflow.com/questions/32043346/square-matrix-inversion-in-c
 * 
 * @param the_matrix float ptr** to NxN matrix
 * @param row_cols uint with nr of rows/cols (aka N)
 * @return success bool with true if matrix could be inverted, false otherwise
 */ 
///////////////////////////////////////////////////////////////////////////////
bool invert_Matrix(float **the_matrix, unsigned int row_cols)
{
    bool inverted_flag= false;
    
    float **Inv = NULL;
    float temp = 0;
    Inv = init_2d_array_float(row_cols, row_cols);
       
    for(unsigned int i=0; i<row_cols; i++)              // initialize the unit matrix, e.g.
        for(unsigned int j=0; j<row_cols; j++)          //  -       -
            if(i==j)                                    // | 1  0  0 |
                Inv[i][j]=1;                            // | 0  1  0 |
            else                                        // | 0  0  1 |
                Inv[i][j]=0;                            //  -       -
                    
    for(unsigned int k=0; k<row_cols; k++)                 
    {                                                
        temp = the_matrix[k][k];                       
        if(temp == 0)
        {
            free_2d_array_float(Inv, row_cols);
            return inverted_flag;
        }
        
        for(unsigned int j=0; j<row_cols; j++)            
        {                                               
            the_matrix[k][j] /= temp;                              
            Inv[k][j] /= temp;                          
        }                                              
           
        for(unsigned int i=0; i<row_cols; i++)                              
        {
            temp=the_matrix[i][k];  
            if(temp == 0)
            {
                free_2d_array_float(Inv, row_cols);
                return inverted_flag;
            }
            
            for(unsigned int j=0; j<row_cols; j++)            
            {                                   
                if(i==k)
                break;                   
                the_matrix[i][j] -= the_matrix[k][j]*temp;         
                Inv[i][j] -= Inv[k][j]*temp;        
            }
        }
    }
        
    for(unsigned int i=0; i< row_cols; i++)
        for(unsigned int j=0; j< row_cols; j++)
            the_matrix[i][j] = Inv[i][j];       
    free_2d_array_float(Inv, row_cols);
    inverted_flag = true;

    return inverted_flag;
}

//end of file
