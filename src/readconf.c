/* readconf.c
 * read config files, init default parameters
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * based on sample code found on 
 * http://www.linuxquestions.org/questions/programming-9/read-parameters-from-config-file-file-parser-362188/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
// 
// ToDo: sanity check for parsed parameters
// update: DO IT DAMMIT! Spend 1h bughunting when days_to_analyze was
// smaller than period_long grml

/**
 * @file readconf.c
 * @brief reads config files, initializes default parameters
 *
 * @todo enhance sanity check for parsed parameters, including numerical
 *       values
 * This file contains the functions which initializes the configuration, read
 * the config files and do some (limited) sanity checking
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "constants.h"
#include "debug.h"
#include "readconf.h"
#include "arrays.h"


///////////////////////////////////////////////////////////////////////////////
/**
 * @brief initialize parameters with default values
 *
 * This function initialize parameters with default values and stores them in 
 * a parameter struct
 * 
 * @param parms pointer to parameter struct
 */
///////////////////////////////////////////////////////////////////////////////
void init_parameters (parameter *parms)
{
	// generate default settings strings
	bstring symbolfilestr = bfromcstr("symbols.conf");	// file that holds symbols (and filenames of .csv)
	bstring currencystr = 	bfromcstr("EUR"); // currency of user account
	bstring hoststr	=	bfromcstr("localhost"); 
	bstring usernamestr=	bfromcstr("defaultuser");
	bstring passwordstr=	bfromcstr("defaultpassword");
	bstring dbnamestr=	bfromcstr("default_database");
	bstring termresultstr = bfromcstr("text");	//human readable 
	bstring termstatstr = 	bfromcstr("none");		// no statistics after execution
	bstring regimefltstr = 	bfromcstr("none"); // Market regime filter: ADX, TNI, none (default)
	bstring exdatestr = 	bfromcstr("today"); // when should signal be executed: signal_date (date when signal was triggered), real_date (date when program is run), signal_next (the day after signal was triggered)
	bstring pyramidstr = 	bfromcstr("none"); //pyramid buy/sell signals, e.g. buy more than 1 position in same direction? none: buy max 1 position in short/max 1 in long position, daily: execute only 1 long and 1 short position per day, pyramid on consecutive days, full: do not impose any limits, buy every single signal
	bstring allocstr = 	bfromcstr("kelly"); // Model to perform portfolio allocation: equal/fixed/kelly
	bstring slstr = 	bfromcstr("percentage");	// SL type: percentage stop (default), chandelier (Chandelier Stop)
	bstring sladjuststr = 	bfromcstr("trailing"); 	// fixed (use initial SL only), trailing (adjust SL in trend direction only), updown (adjust SL in both directions), default: trailing
	bstring colorstr = 	bfromcstr("contrast");
    bstring trans_coststr = bfromcstr("none");
	
	// General settings (overules Major settings because of higher rank)
	parms->CONFIG_SYMBOLFILE = bstrcpy(symbolfilestr);	// file that holds symbols (and filenames of .csv)
	parms->ACCOUNT_CURRENCY = bstrcpy(currencystr); // currency of user account
	parms->ACCOUNT_CURRENCY_DIGITS = 2; 	     // nr of significant digits
    parms->TRANSACTION_COSTS_FIX = 0.0;          // fixed amount fee in account currency
	parms->TRANSACTION_COSTS_PERCENTAGE = 0.0;   // fee as percentage of position´s value
	
	/* database settings */
	parms->DB_SYSTEM_HOSTNAME	= bstrcpy(hoststr); 
	parms->DB_SYSTEM_USERNAME	= bstrcpy(usernamestr);
	parms->DB_SYSTEM_PASSWORD	= bstrcpy(passwordstr);
	parms->DB_SYSTEM_NAME		= bstrcpy(dbnamestr);
	parms->DB_SYSTEM_PORT		= 0;
	parms->DB_SYSTEM_SOCKET	= NULL;
	parms->DB_SYSTEM_FLAGS		= 0;

    parms->DB_QUOTES_HOSTNAME	= bstrcpy(hoststr); 
	parms->DB_QUOTES_USERNAME	= bstrcpy(usernamestr);
	parms->DB_QUOTES_PASSWORD	= bstrcpy(passwordstr);
	parms->DB_QUOTES_NAME		= bstrcpy(dbnamestr);
	parms->DB_QUOTES_PORT		= 0;
	parms->DB_QUOTES_SOCKET	= NULL;
	parms->DB_QUOTES_FLAGS		= 0;
    
	// terminal output settings
	parms->TERMINAL_EVAL_RESULT = bstrcpy(termresultstr);	//human readable 
	parms->TERMINAL_STATISTICS = bstrcpy(termstatstr);	// no statistics after execution
	
	// General indicator settings
	parms->INDI_DAYS_TO_UPDATE=200;
	parms->INDI_ADX_PERIOD=14;	// number of periods in ADX' EMAs
	parms->INDI_TRACK_TREND = true;    // calculate market trends/duration and store them in db
	
	/* Ichimoku specific settings */
	parms->ICHI_DAYS_TO_ANALYZE=60;
	parms->ICHI_PERIOD_SHORT=9;			
	parms->ICHI_PERIOD_MID=26;
	parms->ICHI_PERIOD_LONG=52;
	parms->ICHI_CHIKOU_CONFIRM = true;	// use chikou span as confirmation/filter for trend (default true)
	parms->ICHI_KUMO_CONFIRM = true;	// check whether Kumo breakout is higher/lower than last reaction high/low
	parms->ICHI_EXECUTION_SENKOU_X = true; // execute Signal Senkou Cross, default true
	parms->ICHI_EXECUTION_KIJUN_X = true;  // execute Signal Kijun Cross, default true
	parms->ICHI_EXECUTION_CHIKOU_X = true; // execute Chikou Cross, default true
	parms->ICHI_EXECUTION_TK_X = true;     // execute Tenkan/Kijun Cross, default true
	parms->ICHI_EXECUTION_KUMOBREAK = true; // execute Kumo breakout, default true  
	
	// settings related to signal execution
	parms->SIGNAL_REGIME_FILTER = bstrcpy(regimefltstr); // Market regime filter: ADX, TNI, none (default)
	parms->SIGNAL_REGIME_ADX_THRESH = 30;   // ADX market filter threshold, default 30
	parms->SIGNAL_DAYS_TO_EXECUTE=5;
	parms->SIGNAL_EXECUTION_DATE =bstrcpy(exdatestr); // when should signal be executed: signal_date (date when signal was triggered), real_date (date when program is run), signal_next (the day after signal was triggered)
	parms->SIGNAL_EXECUTION_PYRAMID = bstrcpy(pyramidstr); //pyramid buy/sell signals, e.g. buy more than 1 position in same direction? none: buy max 1 position in short/max 1 in long position, daily: execute only 1 long and 1 short position per day, pyramid on consecutive days, full: do not impose any limits, buy every single signal
	parms->SIGNAL_MANUAL_CONFIRM = true;	// true: confirm execution of signals; false: auto buy/sell
	parms->SIGNAL_EXECUTION_WEAK=false;	// should weak signals be executed?
	parms->SIGNAL_EXECUTION_NEUTRAL=false; // should neutral signals be executed?
	parms->SIGNAL_EXECUTION_STRONG=true;	// should strong signals be executed?
	parms->SIGNAL_EXECUTION_LONG=true;	// should long signals be executed?
	parms->SIGNAL_EXECUTION_SHORT=true;	// should short signals be executed?
	parms->SIGNAL_EXECUTION_SUNDAYS=false; // execute signals that occur on Sundays?
	
	// Portfolio related settings
	parms->PORTFOLIO_ALLOCATION = bstrcpy(allocstr); // Model to perform portfolio allocation: equal/fixed/kelly
    parms->PORTFOLIO_RETURNS_PERIOD = 20;     // period for which mean returns are calulated
    parms->PORTFOLIO_RETURNS_UPDATE = 10;	// update mean returns every X periods
	
	// Risk management settings
	parms->STARTING_BALANCE = 0;	  // starting account balance at begin of backtest/live trading
	parms->RISK_PER_POSITION = 0.02; 	// Risk of a new position in % of current Equity
	parms->POS_SIZE_CAP = 0.1;         // new positions: cap size to x% of current balance + risk free Equity
	parms->RISK_FREE_RATE = 0.02;      //rate of returns for 0 risk investment
	
	// Stop Loss settings
	parms->SL_TYPE = bstrcpy(slstr);		// SL type: percentage stop (default), chandelier (Chandelier Stop)
	parms->SL_PERCENTAGE_RISK = 0.02;		// the SL percentage for SL_TYPE = percentage, default 0.02
	parms->SL_ATR_FACTOR = 3;         	// SL_TYPE=chandelier: Place the SL x-times the ATR away (default 3)
	parms->SL_ATR_PERIOD = 26;        	// SL_TYPE=chandelier: Period for Averaging the True Range (default 26)	
	parms->SL_ADJUST = bstrcpy(sladjuststr); 	// fixed (use initial SL only), trailing (adjust SL in trend direction only), updown (adjust SL in both directions), default: trailing
    parms->SL_SAVE_DB = false;                  // save all SL records in mysql database? (true/false)
	
	// chart plotting settings
	parms->CHART_PLOT	= true;
	parms->CHART_PLOT_WEAK = false;	        // plot weak signals; default false
	parms->CHART_PLOT_NEUTRAL = false;	// plot neutral signals; default false
	parms->CHART_PLOT_STRONG = true;	// plot strong signals; default true
	parms->CHART_COLORSCHEME= bstrcpy(colorstr);
	parms->CHART_DAYS_TO_PLOT = 120;
	
	bdestroy(symbolfilestr);
	bdestroy(currencystr);
	bdestroy(hoststr);
	bdestroy(usernamestr);
	bdestroy(passwordstr);
	bdestroy(dbnamestr);
	bdestroy(termresultstr);
	bdestroy(termstatstr);
	bdestroy(regimefltstr);
	bdestroy(exdatestr);
	bdestroy(pyramidstr);
	bdestroy(allocstr);
	bdestroy(slstr);
	bdestroy(sladjuststr);
	bdestroy(colorstr);
    bdestroy(trans_coststr);
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief Destroy parameter struct and free memory
 *
 * Destroy parameter struct and free memory
 * 
 * @param parms pointer to parameter struct
 * @return 1 if successfull, 1 otherwise
 */
///////////////////////////////////////////////////////////////////////////////
int destroy_parameters(parameter *parms)
{
	bdestroy(parms->CONFIG_SYMBOLFILE);
	bdestroy(parms->ACCOUNT_CURRENCY);
	bdestroy(parms->DB_SYSTEM_HOSTNAME);
	bdestroy(parms->DB_SYSTEM_USERNAME);
	bdestroy(parms->DB_SYSTEM_PASSWORD);
	bdestroy(parms->DB_SYSTEM_NAME);
	bdestroy(parms->DB_SYSTEM_SOCKET);
    bdestroy(parms->DB_QUOTES_HOSTNAME);
	bdestroy(parms->DB_QUOTES_USERNAME);
	bdestroy(parms->DB_QUOTES_PASSWORD);
	bdestroy(parms->DB_QUOTES_NAME);
	bdestroy(parms->DB_QUOTES_SOCKET);
	bdestroy(parms->TERMINAL_EVAL_RESULT);
	bdestroy(parms->TERMINAL_STATISTICS);
	bdestroy(parms->SIGNAL_REGIME_FILTER);
	bdestroy(parms->SIGNAL_EXECUTION_DATE);
	bdestroy(parms->SIGNAL_EXECUTION_PYRAMID);
	bdestroy(parms->PORTFOLIO_ALLOCATION);
	bdestroy(parms->SL_TYPE);
	bdestroy(parms->SL_ADJUST);
	
	bdestroy(parms->CHART_COLORSCHEME);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief parse config file, set parameters accordingly
 *
 * This function opens the config file, parses it, does some (limited) sanity
 * checking of configuration and stores the configured parameters into 
 * parameter struct
 * 
 * @param filename bstring containing config file name
 * @param parms pointer to parameter struct
 * @return 0 if successfull, 1 otherwise
 */
///////////////////////////////////////////////////////////////////////////////
int parse_config(bstring filename, parameter *parms)
{
	bstring line=NULL;
	bstring name=NULL;
	bstring tmpvalue=NULL;
	int i;
	int warnings;
	
	FILE *fp = fopen(bdata(filename), "r");
	
	if(fp == NULL)
	{
		log_warn("Config file %s not found! Using default values...\n", bdata(filename));
		return 1;
	}

	for(warnings = 0; 
		NULL != (line = bgets ((bNgetc) fgetc, fp, (char) '\n')); 
		bdestroy(line))
	{
		/* Remove extraneous whitespace & trailing \n */
		btrimws(line); 

		if( (bstrchr(line, '#')==0) || (bstrchr(line, '[')==0) )  // omit comment lines starting with #
			continue;
		if (0 > (i = bstrchr(line, '=')))  // i stores position of '='
		{ 
			/* Search for = character */
			if (blength (line)) 
			{
				log_warn("Ignoring syntax error in configfile: \"%s\". Should contain \"<var>=<parm>\" (comments starting with #)", bdata(line));
				warnings++;
			}
			continue;
		}
		
		// looks complicated, but is needed to prevent memory leak
		// create bstring ptr to original bstring content
		// modify bstring and then free temporary pointer
		bstring *tmpstr = init_1d_array_bstring(1);
		*tmpstr = name;
		name = bmidstr(line, 0, i); /* everything left of the = */
		btrimws(name);
		free_1d_array_bstring(tmpstr,1);
		
		// if line contains comment after values, cut them off
		int j=0;
		if (0 < (j = bstrchr(line, '#')))
			bassignmidstr(line,line, 0,j);

		/* Copy into correct entry in parameters struct */
		
		//////////////////////////////////////////////////////////////////////
		// General settings (overules Major settings because of higher rank)
		//////////////////////////////////////////////////////////////////////
	 	if (biseqcstr (name, "CONFIG_SYMBOLFILE")) 	// file that holds symbols (and filenames of .csv)
 		{
 			bassignmidstr (parms->CONFIG_SYMBOLFILE, line, i+1, blength (line));
			btrimws (parms->CONFIG_SYMBOLFILE);
 		}
		else if(biseqcstr (name, "ACCOUNT_CURRENCY")) 
 		{
 			bassignmidstr (parms->ACCOUNT_CURRENCY, line, i+1, blength (line));
			btrimws (parms->ACCOUNT_CURRENCY);
			if(check_parameter(parms, name, parms->ACCOUNT_CURRENCY))
				warnings++;
 		}
 		else if(biseqcstr (name, "ACCOUNT_CURRENCY_DIGITS"))	// nr of significant digits
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			parms->ACCOUNT_CURRENCY_DIGITS = atoi(bdatae(tmp,"???"));
			bdestroy(tmp);	
		}
        else if (biseqcstr (name, "TRANSACTION_COSTS_FIX"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);  
			parms->TRANSACTION_COSTS_FIX = atof(bdatae(tmp,"???"));
			bdestroy(tmp);
		}
        else if (biseqcstr (name, "TRANSACTION_COSTS_PERCENTAGE"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);  
			parms->TRANSACTION_COSTS_PERCENTAGE = atof(bdatae(tmp,"???"));
			bdestroy(tmp);
		}
		
		//////////////////////////////////////////////////////////////////////
		/* MySQL related settings */
		//////////////////////////////////////////////////////////////////////
 		else if (biseqcstr (name, "DB_SYSTEM_HOSTNAME")) 
 		{
 			bassignmidstr (parms->DB_SYSTEM_HOSTNAME, line, i+1, blength (line));
			btrimws (parms->DB_SYSTEM_HOSTNAME);
 		}
		else if (biseqcstr (name, "DB_SYSTEM_USERNAME")) 
		{
			bassignmidstr (parms->DB_SYSTEM_USERNAME, line, i+1, blength (line));
			btrimws (parms->DB_SYSTEM_USERNAME);
		}
		else if (biseqcstr (name, "DB_SYSTEM_PASSWORD")) 
		{
			bassignmidstr (parms->DB_SYSTEM_PASSWORD, line, i+1, blength (line));
			btrimws (parms->DB_SYSTEM_PASSWORD);
		}
		else if (biseqcstr (name, "DB_SYSTEM_NAME")) 
		{
			bassignmidstr (parms->DB_SYSTEM_NAME, line, i+1, blength (line));
			btrimws (parms->DB_SYSTEM_NAME);
		}
		else if (biseqcstr (name, "DB_SYSTEM_PORT")) 
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			parms->DB_SYSTEM_PORT = atoi(bdatae(tmp,"???"));
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "DB_SYSTEM_SOCKET")) 
		{
			bassignmidstr (parms->DB_SYSTEM_SOCKET, line, i+1, blength (line));
			btrimws (parms->DB_SYSTEM_SOCKET);
		}
		else if (biseqcstr (name, "DB_SYSTEM_FLAGS")) 
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			parms->DB_SYSTEM_FLAGS = atoi(bdatae(tmp,"???"));      
			bdestroy(tmp);
		}
		
		else if (biseqcstr (name, "DB_QUOTES_HOSTNAME")) 
 		{
 			bassignmidstr (parms->DB_QUOTES_HOSTNAME, line, i+1, blength (line));
			btrimws (parms->DB_QUOTES_HOSTNAME);
 		}
		else if (biseqcstr (name, "DB_QUOTES_USERNAME")) 
		{
			bassignmidstr (parms->DB_QUOTES_USERNAME, line, i+1, blength (line));
			btrimws (parms->DB_QUOTES_USERNAME);
		}
		else if (biseqcstr (name, "DB_QUOTES_PASSWORD")) 
		{
			bassignmidstr (parms->DB_QUOTES_PASSWORD, line, i+1, blength (line));
			btrimws (parms->DB_QUOTES_PASSWORD);
		}
		else if (biseqcstr (name, "DB_QUOTES_NAME")) 
		{
			bassignmidstr (parms->DB_QUOTES_NAME, line, i+1, blength (line));
			btrimws (parms->DB_QUOTES_NAME);
		}
		else if (biseqcstr (name, "DB_QUOTES_PORT")) 
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			parms->DB_QUOTES_PORT = atoi(bdatae(tmp,"???"));
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "DB_QUOTES_SOCKET")) 
		{
			bassignmidstr (parms->DB_QUOTES_SOCKET, line, i+1, blength (line));
			btrimws (parms->DB_QUOTES_SOCKET);
		}
		else if (biseqcstr (name, "DB_QUOTES_FLAGS")) 
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			parms->DB_QUOTES_FLAGS = atoi(bdatae(tmp,"???"));      
			bdestroy(tmp);
		}
		
		
		
		//////////////////////////////////////////////////////////////////////
		// terminal output settings
		//////////////////////////////////////////////////////////////////////
		else if (biseqcstr (name, "TERMINAL_EVAL_RESULT"))
		{
			bassignmidstr (parms->TERMINAL_EVAL_RESULT, line, i+1, blength(line));
			btrimws (parms->TERMINAL_EVAL_RESULT);
			if(check_parameter(parms, name, parms->TERMINAL_EVAL_RESULT))
				warnings++;
		}
		else if (biseqcstr (name, "TERMINAL_STATISTICS"))
		{
			bassignmidstr (parms->TERMINAL_STATISTICS, line, i+1, blength(line));
			btrimws (parms->TERMINAL_STATISTICS);
			if(check_parameter(parms, name, parms->TERMINAL_STATISTICS))
				warnings++;
		}
				
		//////////////////////////////////////////////////////////////////////
		// General indicator settings
		//////////////////////////////////////////////////////////////////////
 		else if (biseqcstr (name, "INDI_DAYS_TO_UPDATE")) 
		{
			bstring tmp;
 			tmp=bmidstr(line, i+1, blength (line));
 			btrimws(tmp);
 			parms->INDI_DAYS_TO_UPDATE = atoi(bdatae(tmp,"???"));
			bdestroy(tmp);
		} 
		else if (biseqcstr (name, "INDI_ADX_PERIOD")) 
		{
			bstring tmp;
 			tmp=bmidstr(line, i+1, blength (line));
 			btrimws(tmp);
 			parms->INDI_ADX_PERIOD = atoi(bdatae(tmp,"???"));
			bdestroy(tmp);
		} 
		else if (biseqcstr (name, "INDI_TRACK_TREND"))
        {
            bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			if(check_parameter(parms, name, tmp))
				warnings++;
			if(biseqcstr(tmp, "true"))
				parms->INDI_TRACK_TREND = true;
			else if(biseqcstr(tmp, "false"))
				parms->INDI_TRACK_TREND = false;
			bdestroy(tmp);  
        }
		
		//////////////////////////////////////////////////////////////////////
		/* 	Ichimoku related settings */        
		//////////////////////////////////////////////////////////////////////
		else if (biseqcstr (name, "ICHI_DAYS_TO_ANALYZE")) 
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			parms->ICHI_DAYS_TO_ANALYZE = atoi(bdatae(tmp,"???"));
			bdestroy(tmp);
		} 
		else if (biseqcstr (name, "ICHI_PERIOD_SHORT")) 
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp); 
			parms->ICHI_PERIOD_SHORT = atoi(bdatae(tmp,"???"));
			bdestroy(tmp);
		} 
		else if (biseqcstr (name, "ICHI_PERIOD_MID"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			parms->ICHI_PERIOD_MID = atoi(bdatae(tmp,"???"));
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "ICHI_PERIOD_LONG")) 
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);  
			parms->ICHI_PERIOD_LONG = atoi(bdatae(tmp,"???"));
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "ICHI_CHIKOU_CONFIRM"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			if(check_parameter(parms, name, tmp))
				warnings++;
			if(biseqcstr(tmp, "true"))
				parms->ICHI_CHIKOU_CONFIRM = true;
			else if(biseqcstr(tmp, "false"))
				parms->ICHI_CHIKOU_CONFIRM = false;
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "ICHI_KUMO_CONFIRM"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			if(check_parameter(parms, name, tmp))
				warnings++;
			if(biseqcstr(tmp, "true"))
				parms->ICHI_KUMO_CONFIRM = true;
			else if(biseqcstr(tmp, "false"))
				parms->ICHI_KUMO_CONFIRM = false;
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "ICHI_EXECUTION_SENKOU_X"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			if(check_parameter(parms, name, tmp))
				warnings++;
			if(biseqcstr(tmp, "true"))
				parms->ICHI_EXECUTION_SENKOU_X = true;
			else if(biseqcstr(tmp, "false"))
				parms->ICHI_EXECUTION_SENKOU_X = false;
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "ICHI_EXECUTION_KIJUN_X"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			if(check_parameter(parms, name, tmp))
				warnings++;
			if(biseqcstr(tmp, "true"))
				parms->ICHI_EXECUTION_KIJUN_X = true;
			else if(biseqcstr(tmp, "false"))
				parms->ICHI_EXECUTION_KIJUN_X = false;
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "ICHI_EXECUTION_CHIKOU_X"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			if(check_parameter(parms, name, tmp))
				warnings++;
			if(biseqcstr(tmp, "true"))
				parms->ICHI_EXECUTION_CHIKOU_X = true;
			else if(biseqcstr(tmp, "false"))
				parms->ICHI_EXECUTION_CHIKOU_X = false;
			bdestroy(tmp);
		}		
		else if (biseqcstr (name, "ICHI_EXECUTION_TK_X"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			if(check_parameter(parms, name, tmp))
				warnings++;
			if(biseqcstr(tmp, "true"))
				parms->ICHI_EXECUTION_TK_X = true;
			else if(biseqcstr(tmp, "false"))
				parms->ICHI_EXECUTION_TK_X = false;
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "ICHI_EXECUTION_KUMOBREAK"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			if(check_parameter(parms, name, tmp))
				warnings++;
			if(biseqcstr(tmp, "true"))
				parms->ICHI_EXECUTION_KUMOBREAK = true;
			else if(biseqcstr(tmp, "false"))
				parms->ICHI_EXECUTION_KUMOBREAK = false;
			bdestroy(tmp);
		}
		//////////////////////////////////////////////////////////////////////
		// settings related to signal execution
		//////////////////////////////////////////////////////////////////////
		else if (biseqcstr (name, "SIGNAL_REGIME_FILTER"))
		{
			bassignmidstr( parms->SIGNAL_REGIME_FILTER, line, i+1, blength(line));
			btrimws (parms->SIGNAL_REGIME_FILTER);
			if(check_parameter(parms, name, parms->SIGNAL_REGIME_FILTER))
				warnings++;
		}
		else if (biseqcstr (name, "SIGNAL_REGIME_ADX_THRESH")) 
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);  
			parms->SIGNAL_REGIME_ADX_THRESH = atoi(bdatae(tmp,"???"));
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "SIGNAL_DAYS_TO_EXECUTE")) 
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);  
			parms->SIGNAL_DAYS_TO_EXECUTE = atoi(bdatae(tmp,"???"));
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "SIGNAL_EXECUTION_DATE"))
		{
			bassignmidstr( parms->SIGNAL_EXECUTION_DATE, line, i+1, blength(line));
			btrimws (parms->SIGNAL_EXECUTION_DATE);
			if(check_parameter(parms, name, parms->SIGNAL_EXECUTION_DATE))
				warnings++;
		}
		else if (biseqcstr (name, "SIGNAL_EXECUTION_PYRAMID"))
		{
			bassignmidstr( parms->SIGNAL_EXECUTION_PYRAMID, line, i+1, blength(line));
			btrimws (parms->SIGNAL_EXECUTION_PYRAMID);
			if(check_parameter(parms, name, parms->SIGNAL_EXECUTION_PYRAMID))
				warnings++;
		}
		else if (biseqcstr (name, "SIGNAL_MANUAL_CONFIRM"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			if(check_parameter(parms, name, tmp))
				warnings++;
			if(biseqcstr(tmp, "true"))
				parms->SIGNAL_MANUAL_CONFIRM = true;
			else if(biseqcstr(tmp, "false"))
				parms->SIGNAL_MANUAL_CONFIRM = false;
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "SIGNAL_EXECUTION_WEAK"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			if(check_parameter(parms, name, tmp))
				warnings++;
			if(biseqcstr(tmp, "true"))
				parms->SIGNAL_EXECUTION_WEAK = true;
			else if(biseqcstr(tmp, "false"))
				parms->SIGNAL_EXECUTION_WEAK = false;
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "SIGNAL_EXECUTION_NEUTRAL"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			if(check_parameter(parms, name, tmp))
				warnings++;
			if(biseqcstr(tmp, "true"))
				parms->SIGNAL_EXECUTION_NEUTRAL = true;
			else if(biseqcstr(tmp, "false"))
				parms->SIGNAL_EXECUTION_NEUTRAL = false;
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "SIGNAL_EXECUTION_STRONG"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			if(check_parameter(parms, name, tmp))
				warnings++;
			if(biseqcstr(tmp, "true"))
				parms->SIGNAL_EXECUTION_STRONG = true;
			else if(biseqcstr(tmp, "false"))
				parms->SIGNAL_EXECUTION_STRONG = false;
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "SIGNAL_EXECUTION_LONG"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			if(check_parameter(parms, name, tmp))
				warnings++;			
			if(biseqcstr(tmp, "true"))
				parms->SIGNAL_EXECUTION_LONG = true;
			else if(biseqcstr(tmp, "false"))
				parms->SIGNAL_EXECUTION_LONG = false;
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "SIGNAL_EXECUTION_SHORT"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			if(check_parameter(parms, name, tmp))
				warnings++;
			if(biseqcstr(tmp, "true"))
				parms->SIGNAL_EXECUTION_SHORT = true;
			else if(biseqcstr(tmp, "false"))
				parms->SIGNAL_EXECUTION_SHORT = false;
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "SIGNAL_EXECUTION_SUNDAYS"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			if(check_parameter(parms, name, tmp))
				warnings++;
			if(biseqcstr(tmp, "true"))
				parms->SIGNAL_EXECUTION_SUNDAYS = true;
			else if(biseqcstr(tmp, "false"))
				parms->SIGNAL_EXECUTION_SUNDAYS = false;
			bdestroy(tmp);
		}
		//////////////////////////////////////////////////////////////////////
		// Portfolio related settings
		//////////////////////////////////////////////////////////////////////
		else if (biseqcstr (name, "PORTFOLIO_ALLOCATION"))
		{
			bassignmidstr( parms->PORTFOLIO_ALLOCATION, line, i+1, blength(line));
			btrimws (parms->PORTFOLIO_ALLOCATION);
			if(check_parameter(parms, name, parms->PORTFOLIO_ALLOCATION))
				warnings++;
		}	
		else if (biseqcstr (name, "PORTFOLIO_RETURNS_PERIOD"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);  
			parms->PORTFOLIO_RETURNS_PERIOD = atoi(bdatae(tmp,"???"));
			bdestroy(tmp);
		}		
		else if (biseqcstr (name, "PORTFOLIO_RETURNS_UPDATE"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);  
			parms->PORTFOLIO_RETURNS_UPDATE = atoi(bdatae(tmp,"???"));
			bdestroy(tmp);
		}		
		/////////////////////////////////////////////////////////////////////
		// Stop Loss settings
		//////////////////////////////////////////////////////////////////////
		else if (biseqcstr (name, "SL_TYPE"))
		{
			bassignmidstr (parms->SL_TYPE, line, i+1, blength(line));
			btrimws(parms->SL_TYPE);	
			if(check_parameter(parms, name, parms->SL_TYPE))
				warnings++;
		}
		else if (biseqcstr (name, "SL_PERCENTAGE_RISK"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);  
			parms->SL_PERCENTAGE_RISK = atof(bdatae(tmp,"???"));
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "SL_ADJUST"))
		{
			bassignmidstr (parms->SL_ADJUST, line, i+1, blength(line));
			btrimws(parms->SL_ADJUST);
			if(check_parameter(parms, name, parms->SL_ADJUST))
				warnings++;
		}
		else if (biseqcstr (name, "SL_ATR_FACTOR"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);  
			parms->SL_ATR_FACTOR = atof(bdatae(tmp,"???"));
			bdestroy(tmp);
		}		
		else if (biseqcstr (name, "SL_ATR_PERIOD"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);  
			parms->SL_ATR_PERIOD = atoi(bdatae(tmp,"???"));
			bdestroy(tmp);
		}
		else if (biseqcstr(name, "SL_SAVE_DB"))
        {
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			if(check_parameter(parms, name, tmp))
				warnings++;
			if(biseqcstr(tmp, "true"))
				parms->SL_SAVE_DB = true;
			else if(biseqcstr(tmp, "false"))
				parms->SL_SAVE_DB = false;
			bdestroy(tmp);
		}
		
		//////////////////////////////////////////////////////////////////////
		// Risk management settings
		//////////////////////////////////////////////////////////////////////
		else if (biseqcstr (name, "STARTING_BALANCE"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);  
			parms->STARTING_BALANCE = atof(bdatae(tmp,"???"));
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "RISK_PER_POSITION"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);  
			parms->RISK_PER_POSITION = atof(bdatae(tmp,"???"));
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "POS_SIZE_CAP"))
        {
 			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);  
			parms->POS_SIZE_CAP = atof(bdatae(tmp,"???"));
			bdestroy(tmp);                 
        }
		
		else if (biseqcstr (name, "RISK_FREE_RATE"))
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);  
			parms->RISK_FREE_RATE = atof(bdatae(tmp,"???"));
			bdestroy(tmp);
		}	

		//////////////////////////////////////////////////////////////////////
		// Chart related settings
		//////////////////////////////////////////////////////////////////////
		else if (biseqcstr (name, "CHART_PLOT")) 
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			if(check_parameter(parms, name, tmp))
				warnings++;
			if(biseqcstr(tmp, "true"))
				parms->CHART_PLOT = true;
			else if(biseqcstr(tmp, "false"))
				parms->CHART_PLOT = false;
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "CHART_PLOT_WEAK")) 
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			if(check_parameter(parms, name, tmp))
				warnings++;
			if(biseqcstr(tmp, "true"))
				parms->CHART_PLOT_WEAK = true;
			else if(biseqcstr(tmp, "false"))
				parms->CHART_PLOT_WEAK = false;
			bdestroy(tmp);
		}
		else if (biseqcstr (name, "CHART_PLOT_NEUTRAL")) 
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			if(check_parameter(parms, name, tmp))
				warnings++;
			if(biseqcstr(tmp, "true"))
				parms->CHART_PLOT_NEUTRAL = true;
			else if(biseqcstr(tmp, "false"))
				parms->CHART_PLOT_NEUTRAL = false;
			bdestroy(tmp);
		}	
		else if (biseqcstr (name, "CHART_PLOT_STRONG")) 
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);
			if(check_parameter(parms, name, tmp))
				warnings++;
			if(biseqcstr(tmp, "true"))
				parms->CHART_PLOT_STRONG = true;
			else if(biseqcstr(tmp, "false"))
				parms->CHART_PLOT_STRONG = false;
			bdestroy(tmp);
		}		
		else if (biseqcstr (name, "CHART_COLORSCHEME")) 
		{
			bassignmidstr (parms->CHART_COLORSCHEME, line, i+1, blength (line));
			btrimws (parms->CHART_COLORSCHEME);  
			if(check_parameter(parms, name, parms->CHART_COLORSCHEME))
				warnings++;
		}
		else if (biseqcstr (name, "CHART_DAYS_TO_PLOT")) 
		{
			bstring tmp;
			tmp=bmidstr(line, i+1, blength (line));
			btrimws(tmp);  
			parms->CHART_DAYS_TO_PLOT = atoi(bdatae(tmp,"???"));
			bdestroy(tmp);
		}		
		else 
		{
			char * tmp;
			log_warn("Ignoring unknown parameter \"%s\" in configfile", tmp = bstr2cstr (name, (char) '\0'));
			bcstrfree(tmp);
			warnings++;
		}
	}
	
	fclose (fp);
	bdestroy(name);
	bdestroy(tmpvalue);
	bdestroy(line);
	
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief parse symbol file, load symbol names into matrix
 *
 * This function opens the config file for the symbols, parses it,  and stores
 * the configured symbols, and their metadata (like csv filenames) into arrays.
 * The name of the symbol file can be configured in the main configuration file
 * using CONFIG_SYMBOLFILE
 * 
 * @param filename bstring with filename of symbol file
 * @param symbolnames bstring pointer to matrix with names of all markets/symbols
 * @param identifiers bstring pointer to matrix with identifiers for all symbols
 * @param csvfiles bstring pointer to matrix with filenames
 * @param tradable uint pointer to matrix with tradeable information
 * @param ticks_pips float pointer to matrix with minimal stepping size of symbol
 * @param contract_size float pointer to matrix with contract sizes
 * @param weight float ptr to matrix with weights
 * @param curr bstring pointer to matrix with reference currencies of symbols
 * @param type bstring pointer to matrix with type of symbol, e.g. CFDFUT, CFDCUR,STOCK
 * @return nr of read in markets
 */
///////////////////////////////////////////////////////////////////////////////
int parse_symbolfile(bstring filename, bstring **symbolnames, 
		     bstring **identifiers, bstring **csvfiles, unsigned int **tradeable, 
		     float **ticks_pips, float **contract_size, float **weight,
		     bstring **curr, bstring **type)
{
	bstring filepath = bfromcstr("config/");
	bconcat(filepath, filename);
	
	
	FILE *fp = fopen(bdata(filepath), "r");
	
	if(fp == NULL)
	{
		log_err("symbol file %s not found! Aborting...", bdata(filepath));
		exit(EXIT_FAILURE);
	}
	
	bstring line = NULL;
	bstring name = NULL;
	
    char tmp[1024]={0x0};
	int fldcnt=0;
	char arr[nr_columns][40]={0x0};
	int recordcnt=0;	
	
	while(fgets(tmp,sizeof(tmp),fp)!=0) // read a record
	{
        if(tmp[0]=='#')     // skip comment lines
            continue;
        else if(strlen(tmp) < 10)   // magic number :(( A line with less than 10 characters is not considered valid
            continue;
        
        parse(tmp,";",arr,&fldcnt);    /* whack record into fields */
    
		for(int i=0;i<fldcnt;i++)
		{                             
            bstring tmp = bfromcstr(arr[i]);
            btrimws(tmp);   // trim all whitespace left and right of string
           
            // do sanity checks and parsing for symbolfile config
            unsigned int tradblval=0;
            float tickval=0;
            float sizeval=0;
            float weightval=0;
            switch(i)
            {
                case col_symbol:
                    // resize the array to hold next element, then copy it into 
                    (*symbolnames) = resize_1d_array_bstring((*symbolnames), recordcnt + 1);
                    (*symbolnames)[recordcnt] = bstrcpy(tmp);
                    break;
                case col_identifier:
                    (*identifiers) = resize_1d_array_bstring((*identifiers), recordcnt + 1);
                    (*identifiers)[recordcnt] = bstrcpy(tmp);
                    break;
                case col_csv:
                    (*csvfiles) = resize_1d_array_bstring((*csvfiles), recordcnt + 1);
                    (*csvfiles)[recordcnt] = bstrcpy(tmp);
                    break;                       
                case col_tradable:
                    if((!is_uint(bdata(tmp), &tradblval)) || ((tradblval!=0) && (tradblval!=1)))    // make sure entry is valid
                    {
                        log_warn("parse error for symbol %s: tradeable \'%s\' is not a valid number (0/1), omiting line", bdata((*symbolnames)[recordcnt]), bdata(tmp));
                        continue;
                    }
                    (*tradeable) = resize_1d_array_uint((*tradeable), recordcnt + 1);
                    (*tradeable)[recordcnt] = tradblval;
                    break;
                case col_ticks_pips:
                    if(!is_float(bdata(tmp), &tickval))
                    {
                        log_warn("parse error for symbol %s: ticks/pips \'%s\' is not a valid number, omiting line\n", bdata((*symbolnames)[recordcnt]), bdata(tmp));
                        continue;
                    }
                    (*ticks_pips) = resize_1d_array_float((*ticks_pips), recordcnt + 1);
                    (*ticks_pips)[recordcnt] = tickval;
                    break;
                case col_contract_size:
                    if(!is_float(bdata(tmp), &sizeval))
                    {
                        log_warn("parse error for symbol %s: contract size \'%s\' is not a valid number, omiting line\n", bdata((*symbolnames)[recordcnt]), bdata(tmp));
                        continue;
                    }
                    (*contract_size) = resize_1d_array_float((*contract_size), recordcnt + 1);
                    (*contract_size)[recordcnt] = sizeval;
                    break;
                case col_weight:
                    if(!is_float(bdata(tmp), &weightval))
                    {
                        log_warn("parse error for symbol %s: weight \'%s\' is not a valid number, omiting line\n", bdata((*symbolnames)[recordcnt]), bdata(tmp));
                        continue;
                    }
                    (*weight) = resize_1d_array_float((*weight), recordcnt + 1);
                    (*weight)[recordcnt] = weightval;
                    break;
                case col_curr:
                    (*curr) = resize_1d_array_bstring((*curr), recordcnt + 1);
                    (*curr)[recordcnt] = bstrcpy(tmp);
                    break;
                case col_type:
                    (*type) = resize_1d_array_bstring((*type), recordcnt + 1);
                    (*type)[recordcnt] = bstrcpy(tmp);
                    break;
                default:
                    break;

            }
            bdestroy(tmp);
		}
        recordcnt++;
	}
    fclose (fp);
	bdestroy(line);
	bdestroy(name);
	bdestroy(filepath);
	return recordcnt;
}


void parse( char *record, char *delim, char arr[][40],int *fldcnt)
{
    char*p=strtok(record,delim);
    int fld=0;
    
    while(p)
    {
        strcpy(arr[fld],p);
		fld++;
		p=strtok('\0',delim);
	}		
	*fldcnt=fld;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief does limited sanity checks on non numerical config parameters
 *
 * This function checks non numerical config parameters for allowed values 
 * (e.g. a boolean option can only be true/false, for every other string this
 * function will complain). If a non-allowed value is given, an error message
 * will be displayed, the default value for this parameter will be set and 
 * execution of the program continues.
 * 
 * @param parms pointer to parameter struct
 * @param parameter bstring with parameter name 
 * @param value bstring with configured value of current parameter
 * @return 0 if successfull, 1 otherwise
 */
///////////////////////////////////////////////////////////////////////////////
int check_parameter(parameter *parms, bstring parameter, bstring value)
{
	//////////////////////////////////////////////////////////////////////
	// General settings
	//////////////////////////////////////////////////////////////////////
	if(biseqcstr(parameter, "ACCOUNT_CURRENCY"))
	{
		if(biseqcstr(value, "EUR") ||
		   biseqcstr(value, "USD") ||
		   biseqcstr(value, "JPY"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"EUR\"",
			        bdata(value),
			        bdata(parameter));
			bassign(parms->ACCOUNT_CURRENCY, bfromcstr("EUR"));
			return 1;
		}
	}
	//////////////////////////////////////////////////////////////////////
	// terminal output settings
	//////////////////////////////////////////////////////////////////////
	if(biseqcstr(parameter, "TERMINAL_EVAL_RESULT"))
	{
		if (biseqcstr(value, "text") || 
			biseqcstr(value, "html") ||
		    biseqcstr(value, "none"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"text\"", 
			        bdata(value),
			        bdata(parameter));
			bassign(parms->TERMINAL_EVAL_RESULT, bfromcstr("text"));
			return 1;
		}
	}
	if(biseqcstr(parameter, "TERMINAL_STATISTICS"))
	{
		if (biseqcstr(value, "text") || 
			biseqcstr(value, "none"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"none\"", 
			        bdata(value),
			        bdata(parameter));
			bassign(parms->TERMINAL_STATISTICS, bfromcstr("none"));
			return 1;
		}
	}
	//////////////////////////////////////////////////////////////////////
	// General indicator settings
	//////////////////////////////////////////////////////////////////////	
	else if(biseqcstr(parameter, "INDI_TRACK_TREND"))
	{
		if (biseqcstr(value, "true") || 
			biseqcstr(value, "false"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"true\"", 
		        bdata(value),
		        bdata(parameter));
			parms->INDI_TRACK_TREND = true;
			return 1;
		}			
	}		
	
	//////////////////////////////////////////////////////////////////////
	// Ichimoku related settings
	//////////////////////////////////////////////////////////////////////
	else if(biseqcstr(parameter, "ICHI_CHIKOU_CONFIRM"))
	{
		if (biseqcstr(value, "true") || 
			biseqcstr(value, "false"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"true\"", 
		        bdata(value),
		        bdata(parameter));
			parms->ICHI_CHIKOU_CONFIRM = true;
			return 1;
		}			
	}	
	else if(biseqcstr(parameter, "ICHI_KUMO_CONFIRM"))
	{
		if (biseqcstr(value, "true") || 
			biseqcstr(value, "false"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"true\"", 
		        bdata(value),
		        bdata(parameter));
			parms->ICHI_KUMO_CONFIRM = true;
			return 1;
		}			
	}
	else if(biseqcstr(parameter, "ICHI_EXECUTION_SENKOU_X"))
	{
		if (biseqcstr(value, "true") || 
			biseqcstr(value, "false"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"true\"" , 
		        bdata(value),
		        bdata(parameter));
			parms->ICHI_EXECUTION_SENKOU_X = true;
			return 1;
		}			
	}
	else if(biseqcstr(parameter, "ICHI_EXECUTION_KIJUN_X"))
	{
		if (biseqcstr(value, "true") || 
			biseqcstr(value, "false"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"true\"", 
		        bdata(value),
		        bdata(parameter));
			parms->ICHI_EXECUTION_KIJUN_X = true;
			return 1;
		}			
	}
	else if(biseqcstr(parameter, "ICHI_EXECUTION_CHIKOU_X"))
	{
		if (biseqcstr(value, "true") || 
			biseqcstr(value, "false"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"true\"" , 
		        bdata(value),
		        bdata(parameter));
			parms->ICHI_EXECUTION_CHIKOU_X = true;
			return 1;
		}			
	}
	else if(biseqcstr(parameter, "ICHI_EXECUTION_TK_X"))
	{
		if (biseqcstr(value, "true") || 
			biseqcstr(value, "false"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"true\"", 
		        bdata(value),
		        bdata(parameter));
			parms->ICHI_EXECUTION_TK_X = true;
			return 1;
		}			
	}
	else if(biseqcstr(parameter, "ICHI_EXECUTION_KUMOBREAK"))
	{
		if (biseqcstr(value, "true") || 
			biseqcstr(value, "false"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"true\"" , 
		        bdata(value),
		        bdata(parameter));
			parms->ICHI_EXECUTION_KUMOBREAK = true;
			return 1;
		}			
	}
	//////////////////////////////////////////////////////////////////////
	// settings related to signal execution
	//////////////////////////////////////////////////////////////////////
	else if(biseqcstr(parameter, "SIGNAL_REGIME_FILTER"))
	{
		if (biseqcstr(value, "ADX") || biseqcstr(value, "TNI") ||
			biseqcstr(value, "none")) 
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"none\"", 
		        bdata(value),
		        bdata(parameter));
			bassign(parms->SIGNAL_REGIME_FILTER, bfromcstr("none"));
			return 1;
		}			
	}	
	else if(biseqcstr(parameter, "SIGNAL_EXECUTION_DATE"))
	{
		if (biseqcstr(value, "signal_date") || 
			biseqcstr(value, "signal_next") ||
			biseqcstr(value, "real_date"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"real_date\"", 
		        bdata(value),
		        bdata(parameter));
			bassign(parms->SIGNAL_EXECUTION_DATE, bfromcstr("real_date"));
			return 1;
		}			
	}	
	else if(biseqcstr(parameter, "SIGNAL_MANUAL_CONFIRM"))
	{
		if (biseqcstr(value, "true") || 
			biseqcstr(value, "false"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"false\"", 
		        bdata(value),
		        bdata(parameter));
			parms->SIGNAL_MANUAL_CONFIRM=false;
			return 1;
		}			
	}
	else if(biseqcstr(parameter, "SIGNAL_EXECUTION_PYRAMID"))
	{
		if (biseqcstr(value, "none") || 
			biseqcstr(value, "daily") ||
			biseqcstr(value, "full"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"full\"",  
		        bdata(value),
		        bdata(parameter));
			bassign(parms->SIGNAL_EXECUTION_PYRAMID, bfromcstr("full"));
			return 1;
		}			
	}
	else if(biseqcstr(parameter, "SIGNAL_EXECUTION_WEAK"))
	{
		if (biseqcstr(value, "true") || 
			biseqcstr(value, "false"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"false\"", 
		        bdata(value),
		        bdata(parameter));
			parms->SIGNAL_EXECUTION_WEAK=false;
			return 1;
		}			
	}
	else if(biseqcstr(parameter, "SIGNAL_EXECUTION_NEUTRAL"))
	{
		if (biseqcstr(value, "true") || 
			biseqcstr(value, "false"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"false\"\n" , 
		        bdata(value),
		        bdata(parameter));
			parms->SIGNAL_EXECUTION_NEUTRAL=false;
			return 1;
		}			
	}	
	else if(biseqcstr(parameter, "SIGNAL_EXECUTION_STRONG"))
	{
		if (biseqcstr(value, "true") || 
			biseqcstr(value, "false"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"true\"", 
		        bdata(value),
		        bdata(parameter));
			parms->SIGNAL_EXECUTION_STRONG=true;
			return 1;
		}			
	}	
	else if(biseqcstr(parameter, "SIGNAL_EXECUTION_LONG"))
	{
		if (biseqcstr(value, "true") || 
			biseqcstr(value, "false"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"true\"", 
		        bdata(value),
		        bdata(parameter));
			parms->SIGNAL_EXECUTION_LONG=true;
			return 1;
		}			
	}	
	else if(biseqcstr(parameter, "SIGNAL_EXECUTION_SHORT"))
	{
		if (biseqcstr(value, "true") || 
			biseqcstr(value, "false"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"true\"" , 
		        bdata(value),
		        bdata(parameter));
			parms->SIGNAL_EXECUTION_SHORT=true;
			return 1;
		}			
	}
	else if(biseqcstr(parameter, "SIGNAL_EXECUTION_SUNDAYS"))
	{
		if (biseqcstr(value, "true") || 
			biseqcstr(value, "false"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"false\"", 
		        bdata(value),
		        bdata(parameter));
			parms->SIGNAL_EXECUTION_SHORT=false;
			return 1;
		}			
	}	
	//////////////////////////////////////////////////////////////////////
	// Portfolio related settings
	//////////////////////////////////////////////////////////////////////	
	else if(biseqcstr(parameter, "PORTFOLIO_ALLOCATION"))
	{
		if (biseqcstr(value, "equal")  ||
			biseqcstr(value, "kelly")  ||
			biseqcstr(value, "fixed"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. currently implemented: equal, fixed, kelly. Using default kelly" , 
		        bdata(value),
		        bdata(parameter));
			bassign(parms->SL_TYPE, bfromcstr("kelly"));
			return 1;
		}			
	}		
	
	
	//////////////////////////////////////////////////////////////////////
	// Stop Loss settings
	//////////////////////////////////////////////////////////////////////
	else if(biseqcstr(parameter, "SL_TYPE"))
	{
		if (biseqcstr(value, "percentage") ||
			biseqcstr(value, "chandelier") ||
            biseqcstr(value, "atr"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. currently implemented: percentage, chandelier, atr. Using default percentage", 
		        bdata(value),
		        bdata(parameter));
			bassign(parms->SL_TYPE, bfromcstr("percentage"));
			return 1;
		}			
	}	
	else if(biseqcstr(parameter, "SL_ADJUST"))
	{
		if (biseqcstr(value, "fixed") || 
			biseqcstr(value, "trailing") ||
			biseqcstr(value, "updown"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"trailing\"" , 
		        bdata(value),
		        bdata(parameter));
			bassign(parms->SL_ADJUST, bfromcstr("trailing"));
			return 1;
		}			
	}	
	else if(biseqcstr(parameter, "SL_SAVE_DB"))
	{
		if (biseqcstr(value, "true") || 
			biseqcstr(value, "false"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"false\"", 
		        bdata(value),
		        bdata(parameter));
			parms->SL_SAVE_DB=false;
			return 1;
		}			
	}
	//////////////////////////////////////////////////////////////////////
	// Chart related settings
	//////////////////////////////////////////////////////////////////////
	else if(biseqcstr(parameter, "CHART_PLOT"))
	{
		if (biseqcstr(value, "true") || 
			biseqcstr(value, "false"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"true\"", 
		        bdata(value),
		        bdata(parameter));
			parms->CHART_PLOT=true;
			return 1;
		}			
	}
	else if(biseqcstr(parameter, "CHART_PLOT_WEAK"))
	{
		if (biseqcstr(value, "true") || 
			biseqcstr(value, "false"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"false\"" , 
		        bdata(value),
		        bdata(parameter));
			parms->CHART_PLOT_WEAK=false;
			return 1;
		}			
	}
	else if(biseqcstr(parameter, "CHART_PLOT_NEUTRAL"))
	{
		if (biseqcstr(value, "true") || 
			biseqcstr(value, "false"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"false\"", 
		        bdata(value),
		        bdata(parameter));
			parms->CHART_PLOT_NEUTRAL=false;
			return 1;
		}			
	}
	else if(biseqcstr(parameter, "CHART_PLOT_STRONG"))
	{
		if (biseqcstr(value, "true") || 
			biseqcstr(value, "false"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"true\"" , 
		        bdata(value),
		        bdata(parameter));
			parms->CHART_PLOT_STRONG=false;
			return 1;
		}			
	}	
	else if(biseqcstr(parameter, "CHART_COLORSCHEME"))
	{
		if (biseqcstr(value, "light") || 
			biseqcstr(value, "contrast"))
				return 0;
		else
		{
			log_warn("\"%s\" not valid for parameter %s. Using default: \"contrast\"", 
		        bdata(value),
		        bdata(parameter));
			bassign(parms->CHART_COLORSCHEME, bfromcstr("contrast"));
			return 1;
		}			
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief checks if string contains a float and converts it
 *
 * This function takes a string, checks if it contains a convertable float
 * number and does the conversion
 * @see shamelessly stolen from 
 * https://stackoverflow.com/questions/33621681/how-to-check-if-user-input-is-a-float-number-in-c
 * @param s const char pointer to string
 * @param dest float pointer to converted float
 * @return true if successfull, false otherwise
 */
///////////////////////////////////////////////////////////////////////////////
bool is_float(const char *s, float *dest) 
{
	if (s == NULL) 
		return false;
	
	char *endptr;
	*dest = (float) strtod(s, &endptr);
	
	if (s == endptr) 
		return false; // no conversion;

	// Look at trailing text
	while (isspace((unsigned char ) *endptr))
	endptr++;
	return *endptr == '\0';
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief checks if string contains an uint and converts it
 *
 * This function takes a string, checks if it contains a convertable uint
 * number and does the conversion
 * @see shamelessly stolen from 
 * https://stackoverflow.com/questions/33621681/how-to-check-if-user-input-is-a-float-number-in-c
 * @param s const char pointer to string
 * @param dest uint pointer to converted uint
 * @return true if successfull, false otherwise
 */
///////////////////////////////////////////////////////////////////////////////
bool is_uint(const char *s, unsigned int *dest) 
{
	if (s == NULL) 
		return false;
	
	char *endptr;
	*dest = (unsigned int) strtol(s, &endptr, 0);
	
	if (s == endptr) 
		return false; // no conversion;

	// Look at trailing text
	while (isspace((unsigned char) *endptr))
	endptr++;
	return *endptr == '\0';
}

/* End of file */
