/* export_csv.h
 * declarations for export_csv.c
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file export_csv.h
 * @brief Header file for export_csv.c
 * 
 * This file contains the declarations for export_csv.c
 * @author Denis Zetzmann
 */

#ifndef EXPORTCSV_H
#define EXPORTCSV_H

int create_quote_table(bstring symbol, unsigned int number, ...);
int write_csv(bstring filename, bstring header, bstring *dates, 
		float **quotes,	unsigned int cols, unsigned int rows);

#endif //EXPORTCSV_H

// end of file
