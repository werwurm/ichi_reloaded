/* class_market.h
 * declarations for class_market.c, interface for "market" class
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_market.h
 * @brief Header file for class_market.c, public member declarations
 *
 * This file contains the "public" available data and functions of the market
 * "class". 
 * Public values/functions are accessible like FooObj->BarMethod(FooObj, [..])
 * @author Denis Zetzmann
 */ 

#ifndef CLASS_MARKET_H
#define CLASS_MARKET_H

#include <stdbool.h>

#include "datatypes.h"
#include "bstrlib.h"
#include "class_quote.h"		// markets contain quote objects
#include "class_indicators.h" 	// markets also inherit from indicators

struct _market_private;    /**< opaque forward declaration, this structs contains
				non-public/ private data/functions */

typedef struct _class_market class_market;

// typedefs function pointers to make declaration of struct better readable
typedef void (*destroyMarketFunc)(struct _class_market *);
typedef struct _class_market* (*cloneMarketFunc)(const struct _class_market*);
typedef void (*printMarketInfoFunc)(const struct _class_market *);
typedef void (*printMarketTrendFunc)(const struct _class_market *);
typedef void (*printMarketTrendByIdxFunc)(const struct _class_market *, unsigned int);
typedef void (*printTranslCurrFunc)(const struct _class_market *);
typedef unsigned int (*getNrIndicatorsFunc)(const struct _class_market *);
typedef void (*addNewIndicatorFunc)(struct _class_market*, char* indicator_name, unsigned int nr_quotes, char* ind_descr, char* tablename);
typedef void (*addIndicatorFunc)(struct _class_market*, struct _class_indicators*);
typedef void(*loadIndicatorDBFunc)(struct _class_market*, char* indicator_name);
typedef void(*saveAllIndicatorsDBFunc)(const struct _class_market*);
typedef int(*getIndicatorPosFunc)(const struct _class_market*, char* indicator_name);
typedef void(*copyIndicatorQuotesFunc)(struct _class_market*, char* src, char* dest);
typedef bool(*IndicatorExistsFunc)(const struct _class_market*, char* indicator_name);

typedef void(*assignTranslationCurrencyFunc)(struct _class_market**, unsigned int, struct _class_market*);
typedef float(*getTranslationCurrencyQuotebyIndexFunc)(const struct _class_market*, unsigned int);
typedef unsigned int(*getTranslationCurrencyDaynrbyIndexFunc)(const struct _class_market*, unsigned int);
typedef bstring* (*getTranslationCurrencyDatebyIndexFunc)(const struct _class_market*, unsigned int);
typedef void(*initIndicatorPositionsFunc)(struct _class_market*);

typedef float (*getMeanReturnsFunc)(const struct _class_market*);
typedef float (*getMeanExcessReturnsFunc)(const struct _class_market*);
typedef float (*getVarianceFunc)(const struct _class_market*);
typedef unsigned int(*getLastUpdateIndexFunc)(const struct _class_market*);

typedef bool (*updateMarketStatisticsFunc)(struct _class_market*, unsigned int);

typedef enum _trend_type(*getTrendTypeByIndexFunc)(struct _class_market*, unsigned int);
typedef void (*setTrendTypeByIndexFunc)(struct _class_market*, enum _trend_type, unsigned int);
typedef unsigned int (*getTrendDurationByIndexFunc)(struct _class_market*, unsigned int);
typedef void (*setTrendDurationByIndexFunc)(struct _class_market*, unsigned int, unsigned int);
typedef void (*setTrendPriceByIndexFunc)(struct _class_market*, unsigned int, unsigned int);
typedef bstring* (*getTrendDateByIndexFunc)(struct _class_market*, unsigned int);
typedef unsigned int(*getTrendDaynrByIndexFunc)(struct _class_market*, unsigned int);
typedef float (*getTrendPricebyIndexFunc)(struct _class_market*, unsigned int);
typedef void (*loadTrendStateDBbyIndexFunc)(struct _class_market *, unsigned int);
typedef void (*saveTrendStateDBFunc)(struct _class_market *);

// struct that will hold widely used indicator positions within market 
// objects for faster access (otherwise each time getIndicatorPos() has 
// to be called which can be time consuming, especially in loops
struct positions
{
	int open;
	int close;
	int high;
	int low;
    int returns;
};

struct _class_market
{
    //public part
    // DATA
    class_indicators** Ind; 	/**< inherited vector to members of indicator class */
    struct positions IndPos;	/**< positions of open/close/high/low within Ind */
    bstring* symbol;		/**< market symbol, e.g. DAX,DJI etc */
    bstring* identifier;    /**< identifier (like ISIN) of the market */
    bool tradeable;		/**< flag if market is tradeable by system */
    unsigned int nr_digits;	/**< nr of relevant digits for quotes, indicators, .. */
    float contract_size;	/**< value of one contract in market´s currency */
    float weight;           /**< weight factor of market (in a list of markets and/or portfolio */
    bstring* currency;		/**< currency of this market, e.g. €, $, ... */
    bstring* markettype;	/**< type of market */
    bstring* csvfile;		/**< filename with price data */
    bstring* comment;		/**< arbitrary comments for market */
    bstring* url;		/**< arbitrary url for market */
   
    // METHODS, call like: foo_obj->bar_method(foo_obj)
    destroyMarketFunc destroy; /**< "destructor", see  class_market_destroyImpl() */
    cloneMarketFunc clone; 	/**< cloning one market object into another, creating a deep copy, see class_market_cloneMarketImpl() */
    printMarketInfoFunc printTable;	/**<  prints out quotes in tabular format, see class_market_printTableImpl() */
    printMarketTrendFunc printMarketTrend;  /**< print out trend info for market */
    printMarketTrendByIdxFunc printMarketTrendByIdx;    /**< print out market´s trend info for a specific date */
    printTranslCurrFunc printTranslCurrTable; /**< prints out current market´s translation currency table */
    getNrIndicatorsFunc getNrIndicators; /**< returns nr. of indicators in current market obj, see class_market_getNrIndicatorsImpl() */
    addNewIndicatorFunc addNewIndicator; 	/**< add a new indicator to market obj, see class_market_addNewIndicatorImpl() */
    addIndicatorFunc	addIndicator; 	/**< adds an existing indicator to market obj, see class_market_addIndicatorImpl() */
    IndicatorExistsFunc IndicatorExists; /**< checks if given Indicator name is already present in market obj, see class_market_IndicatorExistsImpl() */
    loadIndicatorDBFunc loadIndicatorDB; /**< load single indicator data from database, see class_market_loadIndicatorDBImpl() */
    saveAllIndicatorsDBFunc saveAllIndicatorsDB; /** save all indicators contained in market object to database, see class_market_saveAllIndicatorsDBImpl() */
    getIndicatorPosFunc getIndicatorPos; /**< position of specific indicator within indicator vector, see class_market_getIndicatorPosImpl() */
    copyIndicatorQuotesFunc copyIndicatorQuotes; /**< copies data of one indicator to another, see class_market_copyIndicatorQuotesImpl() */
    assignTranslationCurrencyFunc assignTranslationCurrency; /**< copies the given translation currency into current market (within private data object), see class_market_assignTranslationCurrencyImpl() */
    getTranslationCurrencyQuotebyIndexFunc getTranslationCurrencyQuotebyIndex; /**< returns the translation currency quote for given index, see  class_market_getTranslationCurrencyQuotebyIndexImpl() */
    getTranslationCurrencyDaynrbyIndexFunc getTranslationCurrencyDaynrbyIndex; /**< returns translation currency daynr for given index, see class_market_getTranslationCurrencyDaynrbyIndexImpl() */
    getTranslationCurrencyDatebyIndexFunc getTranslationCurrencyDatebyIndex;	/**< returns translation currency date for given index, see class_market_getTranslationCurrencyDatebyIndexImpl() */
    initIndicatorPositionsFunc initIndicatorPositions;  /**< initializes Ind.* (which holds positions of ohlc indicators within market, see class_market_initIndicatorPositionsImpl() */
    
    getMeanReturnsFunc	getMeanReturns; /**< get the mean returns of configured period for this market, see  class_market_getMeanReturnsImpl() */
    getMeanExcessReturnsFunc getMeanExcessReturns;  /**< get the mean excess returns for this market, see class_market_getMeanExcessReturnsImpl() */
    getVarianceFunc	getVariance; /**< get variance of mean returns (within configured period) for this market, see class_market_getVarianceImpl() */
    getLastUpdateIndexFunc getLastUpdateIndex; /**< get index of QuoteObj, when last statistics update happened, see class_market_getLastUpdateIndexImpl() */
    updateMarketStatisticsFunc updateStatistics; 	/**< update the statistical measures of this market, see class_market_updateMarketStatisticsImpl() */
    
    getTrendTypeByIndexFunc getTrendTypeByIndex;  /**< returns current trend status of market */
    setTrendTypeByIndexFunc setTrendTypeByIndex;  /**< sets current trend status for market */
    getTrendDurationByIndexFunc getTrendDurationByIndex; /**< gets current trend´s duration */
    setTrendDurationByIndexFunc setTrendDurationByIndex; /**< sets current trend´s duration */
    setTrendPriceByIndexFunc setTrendPriceByIndex;  /**< set last quote, date and daynr in market´s trend_stats info */
    getTrendDateByIndexFunc getTrendDateByIndex; /**< get date from a specific trend record */
    getTrendDaynrByIndexFunc getTrendDaynrByIndex; /**< get daynr from a specific trend record */
    getTrendPricebyIndexFunc getTrendPriceByIndex; /**< get price from a specific trend record */
    loadTrendStateDBbyIndexFunc loadTrendStateDBbyIndex; /**< load the trend state from a given index */
    saveTrendStateDBFunc saveTrendStateDB; /**< save all trend state records to database */
    
    //private part
    struct _market_private *market_private;	/**< opaque pointer to private data and functions */
};

class_market* class_market_init(char* symbol, char* identifier, unsigned int nrOfDigits, bool tradeable_flag, float contract_size, float weight, char* currency, char* markettypestr, char* csvfilename, char* comment, char* url);     // "constructor" for market "objects"

// Abbreviations
#define PrintIndicatorTable(k,l)	Ind[l]->printTable(k->Ind[l])
#define GetMarketCloseQuote(k,l)	Ind[k->getIndicatorPos(k, "close")]->QuoteObj->quotevec[l]
#define GetMarketCloseDaynr(k,l)	Ind[k->getIndicatorPos(k, "close")]->QuoteObj->daynrvec[l]
#define GetMarketCloseDate(k,l)		Ind[k->getIndicatorPos(k, "close")]->QuoteObj->datevec[l]

#endif // CLASS_MARKET_H
// eof
 
