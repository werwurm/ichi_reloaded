/* arrays.c
 * create dynamic 1d and 2d arrays, free used memory after use	
 *
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2021 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file arrays.c
 * @brief routines for managing dynamic arrays
 *
 * This file contains the functions which create, initialize, manipulate
 * and destroy dynamic arrays of various types and dimensions.
 * @author Denis Zetzmann
 */

#include <stdio.h>
#include <stdlib.h>

#include "arrays.h"
#include "constants.h"
#include "bstrlib.h"

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief wrapper for calloc that does error check and reporting in 
 * 	  case of failure
 * 
 * This is a wrapper function for calloc to allow error checking. Note that it
 * should not be used directly but via the wrapper macro ZCALLOC, to utilize
 * the better reporting of filename and line number.
 * 
 * @see: based on code found on 
 * http://stackoverflow.com/questions/7940279/should-we-check-if-memory-allocations-fail
 * @param file name of the source file containing the calling function 
 * @param line int linenumber of the sourcefile that called zmalloc
 * @param N integer with the number of elements to be allocated
 * @param size integer size to be allocated
 * @return void pointer to allocated memory
*/
///////////////////////////////////////////////////////////////////////////////
void *zcalloc(const char *file, int line, int N, int size)
{
   void *ptr = NULL;
   
   ptr = calloc(N, size);

   if(!ptr && N!=0)
   {
      printf(ANSI_COLOR_RED"Memory allocation failed: %d bytes (%s:%d)\n"ANSI_COLOR_RESET, size, file, line);
      exit(EXIT_FAILURE);
   }

   return(ptr);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief create dynamic NxM float array
 * 
 * Allocates memory for dynamic NxM float array. Returns pointer to 
 * array of row pointers, which themselves point to column pointers.
 * Returned arrays can be accessed the usual way array[x][y]
 * Arrays are initialized with 0.
 * @param N int value with first dimension (rows)
 * @param M int value with 2nd dimension (columns)
 * @return float pointer to float array (array of vectors)
*/
///////////////////////////////////////////////////////////////////////////////
float **init_2d_array_float(unsigned int N, unsigned int M) 
{
	unsigned int i;
	float **array=NULL;
	
	array = (float**) ZCALLOC(N,sizeof(float *));

	for(i = 0 ; i < N ; i++)
		array[i] = (float*) ZCALLOC( M,sizeof(float) );

	return array;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief create dynamic NxM unsigned integer array
 * 
 * Allocates memory for dynamic NxM unsigned integer array. Returns pointer to 
 * array of row pointers, which themselves point to column pointers.
 * Returned arrays can be accessed the usual way array[x][y]
 * Arrays are initialized with 0.
 * @param N int value with first dimension (rows)
 * @param M int value with 2nd dimension (columns)
 * @return float pointer to uint array (array of vectors)
*/
///////////////////////////////////////////////////////////////////////////////
unsigned int **init_2d_array_uint(unsigned int N, unsigned int M) 
{
	unsigned int i;
	unsigned int **array=NULL;
	
	array = (unsigned int **)ZCALLOC(N,sizeof(unsigned int *));
	
	for(i = 0 ; i < N ; i++)
		array[i] = (unsigned int*)ZCALLOC( M,sizeof(unsigned int) );
	
	return array;
}
///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief create dynamic NxM bstring array
 * 
 * Allocates memory for dynamic NxM bstring array. Returns pointer to 
 * array of row pointers, which themselves point to column pointers.
 * Returned arrays can be accessed the usual way array[x][y]
 * Arrays are initialized with NULL pointer.
 * @param N int value with first dimension (rows)
 * @param M int value with 2nd dimension (columns)
 * @return float pointer to bstring array (array of vectors)
*/
///////////////////////////////////////////////////////////////////////////////
bstring **init_2d_array_bstring(unsigned int N, unsigned int M)
{
	unsigned int i;
	bstring **array = NULL;
	
	array = (bstring **)ZCALLOC(N,sizeof(bstring *));
	
	for(i=0; i<N; i++)
		array[i] = (bstring *)ZCALLOC(M,sizeof(bstring));
	
	return array;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief create dynamic Nx1 float array (or vector)
 * 
 * Allocates memory for dynamic Nx1 float array (vector). Returns pointer to 
 * array of row pointers, which themselves point to row values
 * Returned arrays can be accessed the usual way array[x].
 * Arrays are initialized with 0.
 * @param N int value with first dimension (rows)
 * @return float pointer to float array (/vector)
*/
///////////////////////////////////////////////////////////////////////////////
float *init_1d_array_float(unsigned int N)
{
	float *array=NULL;
	array = (float *)ZCALLOC(N,sizeof(float));

	return array;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief create dynamic Nx1 unsigned int array (or vector)
 * 
 * Allocates memory for dynamic Nx1 unsigned int array (vector). Returns 
 * pointer to array of row pointers, which themselves point to row values.
 * Returned arrays can be accessed the usual way array[x].
 * Arrays are initialized with 0.
 * @param N int value with first dimension (rows)
 * @return unsigned int pointer to float array (/vector)
*/
///////////////////////////////////////////////////////////////////////////////
unsigned int *init_1d_array_uint(unsigned int N)
{
	unsigned int *array=NULL;
	array = (unsigned int *)ZCALLOC(N,sizeof(unsigned int));
	
	return array;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief create dynamic Nx1 bstring array (or vector)
 * 
 * Allocates memory for dynamic Nx1 bstring array (vector). Returns 
 * pointer to array of row pointers, which themselves point to row values.
 * Returned arrays can be accessed the usual way array[x].
 * Arrays are initialized with NULL pointer.
 * @param N int value with first dimension (rows)
 * @return unsigned int pointer to bstring array (/vector)
*/
///////////////////////////////////////////////////////////////////////////////
bstring *init_1d_array_bstring(unsigned int N)
{
	bstring* array = (bstring*)ZCALLOC(N,sizeof(bstring));
	
	return array;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief create dynamic Nx1 string array (or vector)
 * 
 * Allocates memory for dynamic Nx1 string array (vector). Returns 
 * pointer to array of row pointers, which themselves point to first character.
 * Returned arrays can be accessed the usual way array[x].

 * @note Legacy function of the pre-bstring days. will be removed in the future
 * @todo CHeck if any code still uses that
 * @param N int value with first dimension (rows)
 * @return unsigned int pointer to char array (/vector)
*/
///////////////////////////////////////////////////////////////////////////////
char **init_1d_array_string(unsigned int N) /* Allocate the array */
{
	unsigned int i;
	char **array=NULL;
	array = ZCALLOC(N,sizeof(char *));
	for(i = 0 ; i < N ; i++)
		array[i] = ZCALLOC(256,sizeof(char));
	return array;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief resizes dynamic NxM float array
 * 
 * This allocates additional memory for dynamic NxM float arrays.
 * Returned arrays can be accessed the usual way array[x][y].
 * @param array float ptr to **array
 * @param oldN uint with original nr. of rows
 * @param newN uint with new nr. of rows
 * @param newM uint with new nr. of columns
 * @return float pointer to extended float array
*/
///////////////////////////////////////////////////////////////////////////////
float **resize_2d_array_float(float **array, unsigned int oldN, unsigned int oldM, unsigned int newN, unsigned int newM)
{
    float **resized = NULL;
    
    // extend "pointer to rows" to new row nr
    resized = (float**) realloc(array, sizeof(float*) * newN);
    if(!resized)
    {
        printf(ANSI_COLOR_RED"Memory allocation failed: %lu bytes (%s:%i)\n"ANSI_COLOR_RESET, (sizeof(float) * newM), __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }   
    
    // now extend old existing rows with new columns
    for(unsigned int i=0; i< oldN; ++i)
    {
        resized[i] = (float*) realloc(resized[i], sizeof(float) * newM);
        if(!resized[i])
        {
            printf(ANSI_COLOR_RED"Memory allocation failed: %lu bytes (%s:%i)\n"ANSI_COLOR_RESET, (sizeof(float) * newM), __FILE__, __LINE__);
            exit(EXIT_FAILURE);
        }
    }
    
    // finally for the new rows create all new columns
    for(unsigned int i=oldN; i<newN; ++i)
        resized[i] = (float*) ZCALLOC(newM, sizeof(float));
    
    // initialize new cells with 0.0
    for(unsigned int i=0; i<oldN; ++i)
        for(unsigned int j=oldM; j<newM; ++j)
            resized[i][j] = 0.0;
    for(unsigned int i=oldN; i<newN; ++i)
        for(unsigned int j=0; j<newM; ++j)
            resized[i][j] = 0.0;       
    
    return resized;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief resizes dynamic Nx1 float array/vector
 * 
 * This allocates additional memory for dynamic Nx1 float arrays (vectors).
 * Returned arrays can be accessed the usual way array[x].
 * @param newsize int value with new dimension (number of rows)
 * @return float pointer to float array (/vector)
*/
///////////////////////////////////////////////////////////////////////////////
float *resize_1d_array_float(float *array, int newsize)
{
	float * newarray = NULL;
    newarray = (float*) realloc ( array, newsize * sizeof(float));	
    if(!newarray)
    {
        printf(ANSI_COLOR_RED"Memory allocation failed: %lu bytes (%s:%i)\n"ANSI_COLOR_RESET, (sizeof(float) * newsize), __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    } 
	return newarray;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief resizes dynamic Nx1 bstring array/vector
 * 
 * This allocates additional memory for dynamic Nx1 bstring arrays (vectors).
 * Returned arrays can be accessed the usual way array[x].
 * @param newsize int value with new dimension (number of rows)
 * @return unsigned int pointer to bstring array (/vector)
*/
///////////////////////////////////////////////////////////////////////////////
bstring *resize_1d_array_bstring(bstring *array, int newsize)
{
	bstring * newarray = (bstring*) realloc ( array, newsize * sizeof(bstring));
    if(!newarray)
    {
        printf(ANSI_COLOR_RED"Memory allocation failed: %lu bytes (%s:%i)\n"ANSI_COLOR_RESET, (sizeof(float) * newsize), __FILE__, __LINE__);
        exit(EXIT_FAILURE);        
    }
	return newarray;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief resizes dynamic Nx1 uint array/vector
 * 
 * This allocates additional memory for dynamic Nx1 uint arrays (vectors).
 * Returned arrays can be accessed the usual way array[x].
 * @param newsize int value with new dimension (number of rows)
 * @return unsigned int pointer to uint array (/vector)
*/
///////////////////////////////////////////////////////////////////////////////
unsigned int *resize_1d_array_uint(unsigned int *array, int newsize)
{
	unsigned int * newarray = (unsigned int*) realloc ( array, newsize * sizeof(unsigned int));	
    if(!newarray)
    {
        printf(ANSI_COLOR_RED"Memory allocation failed: %lu bytes (%s:%i)\n"ANSI_COLOR_RESET, (sizeof(float) * newsize), __FILE__, __LINE__);
        exit(EXIT_FAILURE);        
    }    
	return newarray;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief free memory of dynamic 2d float array
 * 
 * This function frees allocated memory for NxM float array.
 * @param array pointer to float matrix to be freed
 * @param N int value with dimension (number of rows)
*/
///////////////////////////////////////////////////////////////////////////////
void free_2d_array_float(float **array, unsigned int N)
{
	unsigned int i;
	for(i = 0 ; i < N ; i++)
		free(array[i]);
	free(array);
    array=NULL;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief free memory of dynamic 2d unsigned int array
 * 
 * This function frees allocated memory for NxM unsigned int array.
 * @param array pointer to unsigned int matrix to be freed
 * @param N int value with dimension (number of rows)
*/
///////////////////////////////////////////////////////////////////////////////
void free_2d_array_uint(unsigned int **array, unsigned int N)
{
	unsigned int i;
	for(i = 0 ; i < N ; i++)
		free(array[i]);
	free(array);
    array=NULL;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief free memory of dynamic 2d bstring array
 * 
 * This function frees allocated memory for NxM unsigned bstring array.
 * @param array pointer to bstring array to be freed
 * @param N unsigned int value with first dimension (number of rows)
 * @param M unsigned int value with second dimension (number of columns)
*/
///////////////////////////////////////////////////////////////////////////////
void free_2d_array_bstring(bstring **array, unsigned int N, unsigned int M)
{ 
	unsigned int i,j;
	for(i=0; i<N; i++)
		for(j=0; j<M; j++)
			bdestroy(array[i][j]);
	free(array);	
    array=NULL;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief free memory of dynamic 1d float array (/vector)
 * 
 * This function frees allocated memory for Nx1 float array.
 * @param array pointer to bstring array to be freed
*/
///////////////////////////////////////////////////////////////////////////////
void free_1d_array_float(float *array)
{
	free(array);
    array=NULL;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief free memory of dynamic 1d unsigned int array (/vector)
 * 
 * This function frees allocated memory for Nx1 uint array.
 * @param array pointer to uint array to be freed
*/
///////////////////////////////////////////////////////////////////////////////
void free_1d_array_uint(unsigned int *array)
{
	free(array);
    array=NULL;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief free memory of dynamic 1d string array (/vector)
 * 
 * This function frees allocated memory for Nx1 string array.
 * @note Legacy function of the pre-bstring days. will be removed in the future
 * @todo CHeck if any code still uses that
 * @param array pointer to char array to be freed
 * @param N unsigned int number of rows
*/
///////////////////////////////////////////////////////////////////////////////
void free_1d_array_char(char **array, unsigned int N)
{
	unsigned int i;
	for(i = 0 ; i < N ; i++)
		free(array[i]);
	free(array);
    array=NULL;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief free memory of dynamic 1d bstring array (/vector)
 * 
 * This function frees allocated memory for Nx1 bstring array.
 * @param array pointer to bstring array to be freed
 * @param N unsigned int number of rows
*/
///////////////////////////////////////////////////////////////////////////////
void free_1d_array_bstring(bstring *array, unsigned int N)
{ 
	unsigned int i;
	for(i=0; i<N; i++)
		bdestroy(array[i]);
	free(array);
    array=NULL;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief Print NxM float array to terminal
 * 
 * This function prints an NxM float array in human readable table form in the
 * terminal.
 * @param array pointer to float array to be printed
 * @param N unsigned int number of rows
 * @param M unsigned int number of columns
*/
///////////////////////////////////////////////////////////////////////////////
void print_2d_array_float(float **array, unsigned int N, unsigned int M) 
{
	unsigned int i, j;
	for(i = 0 ; i < N ; i++)
	{
		for(j = 0 ; j < M ; j++)
			printf("| %.8f  |", array[i][j]);
		printf("\n");    
	}
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief Print Nx1 string array (/vector)  to terminal
 * 
 * This function prints an Nx1 string array in human readable table form in the
 * terminal.
 * @note Legacy function of the pre-bstring days. will be removed in the future
 * @todo CHeck if any code still uses that
 * @param array pointer to string array to be printed
 * @param N unsigned int number of rows
*/
///////////////////////////////////////////////////////////////////////////////
void print_1d_array_char(char **array, unsigned int N)
{
	unsigned int i;
	for(i = 0 ; i < N ; i++)
	{
		printf("%s\n", array[i]);  
	}
}

// End of file 
