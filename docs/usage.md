<img src="pics/logo.png" alt="OTraSys Logo" style="width:50px ; float:left"/>

OTraSys- The Open Trading System  
An open source framework to create trading systems  

Step by Step Usage:
===================
  0. download the program, make sure you have the following folder structure in 
     place:
  ```
     config/
     data/
     data/charts
     docs/
     src/ (you need that only if you want to compile yourself)
     tools/
  ```  
  
   Make sure you have a mysql server up and running, on your machine or on
   one you have access to.
  
  
  1. create a file called `symbols.conf` (or whatever name you like) and  
  configure the symbols/markets you want to analyze, for example:
```
# STOCKS ==============================================================================================================
#symbol;	identifier;	filename;   tradbl?;	ticks/pips;	contr.sz.;	weight;	cur;	type;	comment
# ---------------------------------------------------------------------------------------------------------------------
ADIDAS;		ADS.DE;		ADIDAS.csv;	1;	0.01;		1;		1;	EUR;	STOCK;	#
WALMART;	WAL;		WALMART.csv;	1;	0.01;		1;		1;	USD;    STOCK;  #
# INDICES =============================================================================================================
#symbol;	identifier;	filename;   tradbl?;	ticks/pips;	contr.sz.;	weight;	cur;	type;	comment
# ---------------------------------------------------------------------------------------------------------------------
DAX;		^GDAXI;		DAX.csv;	1;	0.01;		1;		1;	EUR;	CFDFUT;	# DAX30
DJI;		^DJI;		DOW.csv;	1;	0.01;		1;		1;	USD;	CFDFUT;	# Dow Jones
# FUTURES =============================================================================================================
#symbol;	identifier;	filename;   tradbl?;	ticks/pips;	contr.sz.;	weight;	cur;	type;	comment
# ---------------------------------------------------------------------------------------------------------------------
Gold Dec20;	GC=F;		Gold.csv;	1;	0.001;		10;		1;	USD;	CFDFUT;	# Gold Future December '20
Soy Nov20;	S=F;		Soy.csv;	1;	0.1;		1;		1;	USD;	CFDFUT;	# Soybeans Future Nov '20
# FOREX ===============================================================================================================
#symbol;	identifier;	filename;   tradbl?;	ticks/pips;	contr.sz.;	weight;	cur;	type;	comment
# ---------------------------------------------------------------------------------------------------------------------
EURUSD;		EUR/USD;	EURUSD.csv;	0;	0.00001;	100000;		0.2;	EUR;	CFDCUR;	# €/$
EURJPY;	    EUR/JPY;	EURJPY.csv;	0;	0.001;	100000;		0.2;	EUR;	CFDCUR;	# €/ Japanese Yen
```
  2. Set up the database for all quote data: start `tools\setup_quote_db`
  3. Fill the quote database with the price data for your markets of interest.
     (You can get free data from sources like finance.yahoo.com, quandl.com or investing.com)  
     There are various ways to achieve that (using csv files, API of your data provider, ...) 
     If you choose to import csv files, copy them to `data` and run 
     the python script `tools\update_quotes.py` (this scripts assumes the following order:
     `date, open, high, low, close, volume, changes`.
     NOTE: Starting with v0.5.1 OTraSys NO LONGER UPDATES THE QUOTE DATABASE 
     ITSELF! YOU HAVE FEED IN THAT DATA IN EXTERNALLY!  
  4. Copy/rename config/otrasys.conf.EXAMPLE to config/otrasys.conf  
  5. do your configuration, make sure the `# Database related settings` match 
    your mysql configuration, set `CONFIG_SYMBOLFILE` to `symbols.conf` (or
    however you called the file in step 1.). Take a look at 
    `docs/configuration.md` for a complete description of all options.  
  6. to prepare the trading system database, start the program with the `-s` or
    `--setup-db` option:  
    `./otrasys -s`
    (you can repeat this step at any given point to clean your database and
    start with a fresh one). Take a look at docs/database.md if you want to 
    create your database manually (or are interested in the structure).  
  7. finally run the program with or without the `-v`/`--verbose` option:  
    `./otrasys` or
    `./otrasys -v`  
  8. if configured to do so, the GNUPLOT plot files will be in `data/charts`, 
    you can plot them with `gnuplot` _plotfilename_ (or to plot them all, use
    the bash script tools/plotall.sh)  
  9. With invoking the `-r` or `--reports` option you can do the statistics 
     analysis of the generated orderbook entries, which makes sense only if
     you run the program over a longer time period (config option 
     `SIGNAL_DAYS_TO_EXECUTE`).  
  10. For further analysis, you can connect to the database and pull out every
     data you need. See the `tools` folder for same basic examples. For example
     `tools/plot_performance.sh` will plot the system performance automatically.
     <img src="pics/performance.png" alt="sample performance" style="width:800px"/>  
  11. Play with the options, find a system that suits your needs by repeating
     steps 6-10  
  12. Enjoy trading :)  
 
 
 If you are interested in seeing the program in action, visit 
 www.spare-time-trading.de and drop me a comment or a mail: d1z@gmx.de
     
    
