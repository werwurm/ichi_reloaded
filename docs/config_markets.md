<img src="pics/logo.png" alt="OTraSys Logo" style="width:50px ; float:left"/>

OTraSys- The Open Trading System  
An open source framework to create trading systems  

Market configuration
====================
This document describes the configuration of markets for the trading system.  

General information
========================
The configuration of markets is separated from the general config file, because
while the system config is limited to the available parameters, the possible
markets are not. You can feed an arbitrary number of markets into the system, 
if you have the data available (and the computer to handle this amount of 
data).  
The filename of the symbol file can be set via the config parameter 
`CONFIG_SYMBOLFILE` in the general config. A typical symbol config file looks 
like this:  
```
# STOCKS ==============================================================================================================
#symbol;	identifier;	filename;   tradbl?;	ticks/pips;	contr.sz.;	weight;	cur;	type;	comment
# ---------------------------------------------------------------------------------------------------------------------
ADIDAS;		ADS.DE;		ADIDAS.csv;	1;	0.01;		1;		1;	EUR;	STOCK;	#
WALMART;	WAL;		WALMART.csv;	1;	0.01;		1;		1;	USD;    STOCK;  #
# INDICES =============================================================================================================
#symbol;	identifier;	filename;   tradbl?;	ticks/pips;	contr.sz.;	weight;	cur;	type;	comment
# ---------------------------------------------------------------------------------------------------------------------
DAX;		^GDAXI;		DAX.csv;	1;	0.01;		1;		1;	EUR;	CFDFUT;	# DAX30
DJI;		^DJI;		DOW.csv;	1;	0.01;		1;		1;	USD;	CFDFUT;	# Dow Jones
# FUTURES =============================================================================================================
#symbol;	identifier;	filename;   tradbl?;	ticks/pips;	contr.sz.;	weight;	cur;	type;	comment
# ---------------------------------------------------------------------------------------------------------------------
Gold Dec20;	GC=F;		Gold.csv;	1;	0.001;		10;		1;	USD;	CFDFUT;	# Gold Future December '20
Soy Nov20;	S=F;		Soy.csv;	1;	0.1;		1;		1;	USD;	CFDFUT;	# Soybeans Future Nov '20
# FOREX ===============================================================================================================
#symbol;	identifier;	filename;   tradbl?;	ticks/pips;	contr.sz.;	weight;	cur;	type;	comment
# ---------------------------------------------------------------------------------------------------------------------
EURUSD;		EUR/USD;	EURUSD.csv;	0;	0.00001;	100000;		0.2;	EUR;	CFDCUR;	# €/$
EURJPY;	    EUR/JPY;	EURJPY.csv;	0;	0.001;	100000;		0.2;	EUR;	CFDCUR;	# €/ Japanese Yen
```
As you can see it is basically a csv file using semicolons `;` as delimiter. 
You can use whitespace and tabs to structure it to more human-readable table,
the parser will ignore them. Comments are started by a `#` everything that
follows will be also ignored by the parser.
All fields using strings can also contain whitespaces.  

The following chapters will describe all columns of the symbol config file: 
  * [symbol](#symbol)
  * [identifier](#identifier)
  * [filename](#filename)
  * [tradeable](#tradeable)
  * [tick/pip value](#tickvalue)
  * [contract size](#contract_size)
  * [weight](#weight)
  * [cur](#cur)
  * [type](#type)
  * [comment](#comment)
  

symbol <a name="symbol"></a>
----------------
The symbol usually will contain the name of the market, how it will be handled internally, stored in 
the database and printed to the terminal. You can choose whatever you want for a name, but it makes
sense to call the German stock index DAX instead of Fritz :)

identifier <a name="identifier"></a>
----------------
From OTraSys v0.5.1 on, the field `identifier` was added to the configuration. 
This means you can not only use a symbol but also an identifier for a market.
This allows the central repository to hold different data sets for the same 
stock (useful for stocks with Type A/B, or stocks in several currencies, or 
futures with different due dates, or whatever you want).

filename <a name="filename"></a>
----------------
The filename links above symbol to the corresponding .csv data file that should live in /data

tradeable <a name="tradeable"></a>
----------------
This flag sets if the market should be considered for trades or not. It is useful to set a Forex market
to `0` e.g. not tradeable, if you need the currency quote (say if you have another stock in this currency
which is different from the account currency) but do not want to execute trades in this market. In the
above example imagine the account currency being EUR, while there are a couple of markets in USD. So at
runtime the system needs the EUR/USD quote for currency translation of those markets into account currency.
If set to `0`, no signal calculation nor signal execution is performed.

tick/pip value <a name="tickvalue"></a>
----------------
This defines the smallest possible price change of a market. 

contract size <a name="contract_size"></a>
----------------
The size of a Future contract. This equals the change of value if price moves
1 unit in the market´s currency. For example, if the size of a gold futures 
contract on COMEX is 100 ounces, a 1 Dollar move up or down in the price will result
in a 100 Dollar change in the value of the gold futures contract.
Do not mix this up with the position size: in the above example you could buy
2,3..n future contracts and multiply the 100 Dollar value change with the number of
contracts you hold.  
For stocks/ETF/etc. this value is meaningless, however due to the internal 
structure of the software the contract sizes for stocks/ETF/similar markets
should be set to 1 (1€ move results in +/- 1 € change in value).

weight <a name="weight"></a>
----------------
Here you can enter manual weights. Useful if PORTFOLIO_ALLOCATION is set to
`fixed`, for example if you use manual bootstrapping or other methods and do
not want to let the system choose/ vary weights for you.

cur <a name="cur"></a>
----------------
The currency of the market. This should be a 3-letter code. If different from
the account currency, make sure that a currency pair is available, that has the
quotes available to do the conversion. The direction is arbitrary, e.g. for a 
market in USD and account currency you would need either EURUSD or USDEUR to
deliver the quotes for the conversion.
The system will stop with a warning if no translation quote can be found.

type <a name="type"></a>
----------------
Currently the following types of assets are implemented:
  * STOCKS: as the name says, anything that has a quote, is not traded on 
    margin and your p/L is basically `(sell price - buy price) x quantity`.
    This applies to stocks, ETFs, mutual funds, REITs, Bonds, ...  
  * CFDFUT: this is the option for leveraged assets, traded on margin, like
    CFDs, Futures, ...  
  * CFDCUR: choose this if your trading a FOREX market (or the CFD equivalent)  

comment <a name="comment"></a>
----------------
You can add comments to every single line of your symbol file, to clarify an
entry or whatever you want. Start this field with a `#` and it will not be 
parsed and further processed in any way.  
