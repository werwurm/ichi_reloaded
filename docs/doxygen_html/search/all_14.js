var searchData=
[
  ['undefined',['undefined',['../datatypes_8h.html#abc74969e0af4034334768febc7b72564af35387cab0e820dcf7df1ed4702043e1',1,'datatypes.h']]],
  ['update',['update',['../struct__class__portfolio__element.html#ac71eeb0749dfdaf46ad957965a0989ed',1,'_class_portfolio_element']]],
  ['update_5findicators',['update_indicators',['../indicators__general_8c.html#a468a75469b9a35e4393faea42aa1dba1',1,'update_indicators(class_market *the_market):&#160;indicators_general.c'],['../indicators__general_8h.html#a468a75469b9a35e4393faea42aa1dba1',1,'update_indicators(class_market *the_market):&#160;indicators_general.c']]],
  ['updateportfolioequity',['updatePortfolioEquity',['../struct__class__portfolio.html#ae0319409f01795772eed0b95d9d58f06',1,'_class_portfolio']]],
  ['updatestatistics',['updateStatistics',['../struct__class__market.html#a014b5479168da8d363b969625703061f',1,'_class_market::updateStatistics()'],['../struct__class__market__list.html#a631f9fa19b3d40626b428f251616ccd7',1,'_class_market_list::updateStatistics()']]],
  ['updatestatisticsneeded',['updateStatisticsNeeded',['../struct__statistics.html#a7e8205a7c5ad6ba7c6e4c42034df49a7',1,'_statistics']]],
  ['url',['url',['../struct__class__market.html#a39fe2d6cf4a1c7aa013afda55aa874c8',1,'_class_market']]],
  ['utilities_2ec',['utilities.c',['../utilities_8c.html',1,'']]],
  ['utilities_2eh',['utilities.h',['../utilities_8h.html',1,'']]]
];
