var searchData=
[
  ['adx_5fregime_5ffilter',['ADX_regime_filter',['../indicators__general_8c.html#a012122eab5f8868de9f86507fb581ec0',1,'ADX_regime_filter(const class_indicators *adx, const float adx_threshhold, class_indicators *regime_filter):&#160;indicators_general.c'],['../indicators__general_8h.html#a012122eab5f8868de9f86507fb581ec0',1,'ADX_regime_filter(const class_indicators *adx, const float adx_threshhold, class_indicators *regime_filter):&#160;indicators_general.c']]],
  ['atr_5fstop',['atr_stop',['../stoploss_8c.html#ad04b53b14b1fd130f9dadff637dd1cb5',1,'atr_stop(float price, float atr, float atr_factor, signal_type type):&#160;stoploss.c'],['../stoploss_8h.html#ad04b53b14b1fd130f9dadff637dd1cb5',1,'atr_stop(float price, float atr, float atr_factor, signal_type type):&#160;stoploss.c']]]
];
