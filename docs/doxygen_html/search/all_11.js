var searchData=
[
  ['readconf_2ec',['readconf.c',['../readconf_8c.html',1,'']]],
  ['readconf_2eh',['readconf.h',['../readconf_8h.html',1,'']]],
  ['removeelement',['removeElement',['../struct__class__market__list.html#ac0b06edb9e282a9e3061aef32765599b',1,'_class_market_list::removeElement()'],['../struct__class__portfolio.html#a3f788e2722811f654470e4074b71f8f9',1,'_class_portfolio::removeElement()']]],
  ['removenontradeable',['removeNonTradeable',['../struct__class__market__list.html#a3dcee710710a6ad5e8c369f7eafd79e5',1,'_class_market_list']]],
  ['removesignal',['removeSignal',['../struct__class__signal__list.html#a84b9fb12add12972b833bf71e86b25a3',1,'_class_signal_list']]],
  ['resize_5f1d_5farray_5fbstring',['resize_1d_array_bstring',['../arrays_8c.html#a059afbd800c3ac2906b47584bf791e23',1,'resize_1d_array_bstring(bstring *array, int newsize):&#160;arrays.c'],['../arrays_8h.html#a059afbd800c3ac2906b47584bf791e23',1,'resize_1d_array_bstring(bstring *array, int newsize):&#160;arrays.c']]],
  ['resize_5f1d_5farray_5ffloat',['resize_1d_array_float',['../arrays_8c.html#abdcf8e5c892554fdbde198f78f365397',1,'resize_1d_array_float(float *array, int newsize):&#160;arrays.c'],['../arrays_8h.html#abdcf8e5c892554fdbde198f78f365397',1,'resize_1d_array_float(float *array, int newsize):&#160;arrays.c']]],
  ['resize_5f1d_5farray_5fuint',['resize_1d_array_uint',['../arrays_8c.html#ace0ca712d8e56f7ca9cf2ca5f417806e',1,'resize_1d_array_uint(unsigned int *array, int newsize):&#160;arrays.c'],['../arrays_8h.html#ace0ca712d8e56f7ca9cf2ca5f417806e',1,'resize_1d_array_uint(unsigned int *array, int newsize):&#160;arrays.c']]],
  ['resize_5f2d_5farray_5ffloat',['resize_2d_array_float',['../arrays_8c.html#a19cffa80b91450a32481890f96742f77',1,'resize_2d_array_float(float **array, unsigned int oldN, unsigned int oldM, unsigned int newN, unsigned int newM):&#160;arrays.c'],['../arrays_8h.html#a19cffa80b91450a32481890f96742f77',1,'resize_2d_array_float(float **array, unsigned int oldN, unsigned int oldM, unsigned int newN, unsigned int newM):&#160;arrays.c']]],
  ['rf_5fequity',['rf_equity',['../struct__class__portfolio__element.html#a810a4d8cd4d1617b1e287a598d467120',1,'_class_portfolio_element']]],
  ['risk_5ffree_5fequity',['risk_free_equity',['../struct__accounts__private.html#aac64b839547278c60e508ae1de9946ee',1,'_accounts_private::risk_free_equity()'],['../struct__portfolio__private.html#a7e5ec5f750a06ec83d7d1bd530068e77',1,'_portfolio_private::risk_free_equity()']]],
  ['risk_5ffree_5fp_5fl',['risk_free_p_l',['../struct__class__portfolio__element.html#aa2fca51d001da361de5a138f5e23161b',1,'_class_portfolio_element']]],
  ['risk_5ffree_5fvalue',['risk_free_value',['../struct__class__portfolio__element.html#a4af51b0f0d3bbd02af79048e50037c61',1,'_class_portfolio_element']]],
  ['risk_5fper_5fposition',['RISK_PER_POSITION',['../structparameters.html#aaa99d1b47e33ba589005f61a72d5b40f',1,'parameters']]],
  ['runpersistence',['RunPersistence',['../classp1d_1_1Persistence1D.html#afa9e076d614ac0f0e8dd7e63b1c81868',1,'p1d::Persistence1D']]]
];
